<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Product extends CI_Model{

        public function getRows($id = ''){
            $this->db->select('id,name,image,price');
            $this->db->from('tbl_products');
            if($id){
                $this->db->where('id',$id);
                $query = $this->db->get();
                $result = $query->row_array();
            }else{
                $this->db->order_by('name','asc');
                $query = $this->db->get();
                $result = $query->result_array();
            }
            return !empty($result)?$result:false;
        }

        public function insertTransaction($data = array()){
            $insert = $this->db->insert('tbl_payments',$data);
            return $insert ? true : false;
        }
       
        public function insertTransaction123($data = array()){
            $insert = $this->db->insert('tbl_payments_txn',$data);
            return $insert ? true : false;
        }

    }
?>