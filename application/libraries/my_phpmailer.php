<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_PHPMailer {
    public function __construct()
    {
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function load()
    {
        require_once(APPPATH."libraries/phpmailer/PHPMailerAutoload.php");
        $mail = new PHPMailer;
        return $mail;
    }
}