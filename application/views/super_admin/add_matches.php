<?php $db = mysqli_connect(HOST,USER,PASS,DB); ?>
<div class="content-wrapper">
                <nav id="toolbar" class="bg-white">
                    <div class="row no-gutters align-items-center flex-nowrap">
                        <div class="col">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row no-gutters align-items-center justify-content-end">
                                <button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="../images/avatars/profile.jpg">
                                        </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">
                    <div id="e-commerce-products" class="page-layout carded full-width">
                        <div class="top-bg bg-secondary"></div>
                        <div class="page-content-wrapper">
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">
                                <div class="col-12 col-sm">
                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="fa fa-2x fa-cubes"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4"> Add Match Here</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="page-content-card "> 
                                  <div class="custom-scrollbar">  
                                    <div class="col-md-12 mt-5">
                                        <form role="form" action="" id="myForm" method="post" enctype="multipart/form-data">
                                        <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                                <label for="exampleInputMatchType">Match Type</label>
                                                                <select  class="form-control" name="match_type" id="exampleInputMatchType" required>
                                                                        <option value="T20">T20</option>
                                                                </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                        <div class="col-md-12">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputTeams1">Teams 1</label>
                                                                <select  class="form-control" name="teams_id1" id="exampleInputTeams1" required>
                                                                    <option>Select</option>
                                                                    <?php $sql = mysqli_query($db,"SELECT * FROM tbl_teams"); while ($g1 = mysqli_fetch_assoc($sql)) { ?>
                                                                        <option value="<?php echo $g1['teams_id']?>"<?php if(isset($app_data)){ if($app_data[0]['teams_id1'] == $g1['teams_id']){ echo "selected"; }} ?>><?php echo $g1['teams_name'];?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="exampleInputTeams2">Teams 2</label>
                                                                <select  class="form-control" name="teams_id2" id="exampleInputTeams2" required>
                                                                    <option>Select</option>
                                                                    <?php $sql = mysqli_query($db,"SELECT * FROM tbl_teams"); while ($g2 = mysqli_fetch_assoc($sql)) { ?>
                                                                        <option value="<?php echo $g2['teams_id']?>"<?php if(isset($app_data)){ if($app_data[0]['teams_id2'] == $g2['teams_id']){ echo "selected"; }} ?>><?php echo $g2['teams_name'];?></option>
                                                                    <?php } ?>
                                                                </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                                <input type="text" name="cb_match_number" id="exampleInputCBMatchNumber" value="<?php if (isset($app_data)) {echo $app_data[0]['cb_match_number']; } ?>" class="form-control">
                                                                <label for="exampleInputCBMatchNumber">Crickbuzz Match Number :</label>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                                <input type="text" name="match_time" id="exampleInputMatchTime" value="<?php if (isset($app_data)) {echo $app_data[0]['match_time']; } ?>" class="form-control">
                                                                <label for="exampleInputMatchTime">Match Time :</label>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                                <div class="col-md-12">   
                                                    <div class="form-group">                               
                                                        <button type="button" name="back" onClick="javascript:history.go(-1);" class="btn btn-primary"><i 
                                                                class="fa fa-2x fa-chevron-circle-left"> &nbsp;</i>
                                                        </button>
                                                        <button type="submit" name="submit" class="btn btn-secondary">Submit</button>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                  </div>                                          
                                   

                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo URL;?>js/apps/e-commerce/products/products.js"></script>
                </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $("#matches a").addClass('active');
        $('#exampleInputMatchTime').datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'HH:mm:ss',
            stepHour: 1,
            stepMinute: 5,
            stepSecond: 5,
        }); 
    });

    function image_readURL(input) {
        if (input.files && input.files[0]){
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#image').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>