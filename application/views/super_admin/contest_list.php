<?php $db = mysqli_connect(HOST,USER,PASS,DB); ?>
<script> function account(id) { window.location = "<?php echo base_url();?>super_admin/recharge/index/" + id; } </script>
<div class="content-wrapper">
                <nav id="toolbar" class="bg-white">
                    <div class="row no-gutters align-items-center flex-nowrap">
                        <div class="col">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row no-gutters align-items-center justify-content-end">
                                <button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="../images/avatars/profile.jpg">
                                        </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">
                    <div id="e-commerce-products" class="page-layout carded full-width">
                        <div class="top-bg bg-secondary"></div>
                        <div class="page-content-wrapper">
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">
                                <div class="col-12 col-sm">
                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="fa fa-2x fa-cubes"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4">Contest list</div>
                                            <div class="">Total Contest list: <?php echo count($records); ?></div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-auto search-wrapper px-2">
                                        <div class="input-group">
                                            <a href="<?php echo base_url('super_admin/contest/add_contest'); ?>" class="btn btn-light justify-content-end">Add Contest</a>
                                        </div>
                                    </div>
                            </div>
                            <div class="page-content-card"> 
                                <div class="content custom-scrollbar">
                                  <table id="sample-data-table" class="table" style="font-size: 12px;">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Contest Id</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Total Teams</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Entory Fees</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Total Winners</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Total Winnings Price</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Participate</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">status</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Action</span>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php foreach ($records as $r) { ?>
                                                                    <tr>
                                                                        <td><?php echo $r['contest_id']; ?></td>
                                                                        <!-- <?php $g1 = mysqli_fetch_assoc(mysqli_query($db,"SELECT tbl_matches.*, (SELECT tbl_teams.teams_name FROM tbl_teams WHERE tbl_teams.teams_id = tbl_matches.teams_id1) AS teams_name1, (SELECT tbl_teams.teams_name FROM tbl_teams WHERE tbl_teams.teams_id = tbl_matches.teams_id2) AS teams_name2 FROM tbl_matches WHERE tbl_matches.matches_id = '".$r['matches_id']."'")); ?>
                                                                        <td><?php echo $g1['teams_name1'].' Vs '.$g1['teams_name2'].' ( '.$g1['match_type'].' ) <br/> ( '.$g1['match_time'].' )';?></td> -->
                                                                        <td><?php echo $r['users_teams']; ?></td>
                                                                        <td><?php echo $r['entory_fee']; ?></td>
                                                                        <td><?php echo $r['total_winners']; ?></td>
                                                                        <td><?php echo $r['total_winnings_price']; ?></td>
                                                                        <td><?php echo $r['participate_users']; ?></td>
                                                                        <td style="<?php if($r['status'] == 0){ echo 'color: green;';}if($r['status'] == 1){ echo 'color: red;';} ?>">
                                                                            <?php if($r['status'] == 0){ echo 'Pending';}if($r['status'] == 1){ echo 'Completed';} ?>
                                                                        </td>
                                                                        <td>
                                                                            <a class="fa fa-pencil btn btn-primary" href="<?php echo base_url('super_admin/contest/add_contest') . "/" . $r['contest_id']; ?>" style="margin-bottom: 3px"></a>
                                                                            <a class="fa fa-trash btn btn-danger" onClick="dlt_app(<?php echo $r['contest_id']; ?>)" style="margin-bottom: 3px"></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                

                                                                </tbody>
                                                            </table>

                                                            <script type="text/javascript">
                                                                $('#sample-data-table').DataTable({
                                                                    dom       : '<lf<t>ip>',
                                                                    "iDisplayLength": 100,
                                                                });
                                                            </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo URL;?>js/apps/e-commerce/products/products.js"></script>
                </div>
</div>


<script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
    <script type="text/javascript">
    $(document).ready(function (e) {
        $("#contest_list a").addClass('active');
    });

     function dlt_app(contest_id) {
        if (confirm("Are You Sure you want to delete?")) {
            $.ajax({
            type: 'POST',
            url: '<?php echo site_url('super_admin/contest/delete_contest');?>',
            data: {
                contest_id:contest_id
            },
            success: function(data) {
                var res = JSON.parse(data);
               if(res == 1){
                location.reload();
               }else{
                location.reload();
               }
             }
            });
        }
    }
</script>