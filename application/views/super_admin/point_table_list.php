<?php $db = mysqli_connect(HOST,USER,PASS,DB); ?>
<script> function account(id) { window.location = "<?php echo base_url();?>super_admin/recharge/index/" + id; } </script>
<div class="content-wrapper">
                <nav id="toolbar" class="bg-white">
                    <div class="row no-gutters align-items-center flex-nowrap">
                        <div class="col">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row no-gutters align-items-center justify-content-end">
                                <button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="../images/avatars/profile.jpg">
                                        </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">
                    <div id="e-commerce-products" class="page-layout carded full-width">
                        <div class="top-bg bg-secondary"></div>
                        <div class="page-content-wrapper">
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">
                                <div class="col-12 col-sm">
                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="fa fa-2x fa-cubes"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4">Point Table list</div>
                                            <div class="">Total Point Table list: <?php echo count($records); ?></div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-auto search-wrapper px-2">
                                        <div class="input-group">
                                            <a href="<?php echo base_url('super_admin/point_table/add_point_table'); ?>" class="btn btn-light justify-content-end">Add POINT</a>
                                        </div>
                                    </div>
                            </div>
                            <div class="page-content-card"> 
                                <div class="content custom-scrollbar">
                                  <table id="sample-data-table" class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Point Table Id</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">TEXT</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">KEY</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">VALUE</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">+ / -</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Status</span>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php foreach ($records as $r) { ?>
                                                                    <tr>
                                                                        <td><?php echo $r['point_table_id']; ?></td>
                                                                        <td><?php echo $r['point_table_text']; ?></td>
                                                                        <td><?php echo $r['point_table_key']; ?></td>
                                                                        <td><?php echo $r['point_table_value']; ?></td>
                                                                        <td><?php echo $r['point_table_pm']; ?></td>
                                                                        <td>
                                                                            <a class="fa fa-pencil btn btn-primary" href="<?php echo base_url('super_admin/point_table/add_point_table') . "/" . $r['point_table_id']; ?>" style="margin-bottom: 3px"></a>
                                                                            <a class="fa fa-trash btn btn-danger" onClick="dlt_app(<?php echo $r['point_table_id']; ?>)" style="margin-bottom: 3px"></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                

                                                                </tbody>
                                                            </table>

                                                            <script type="text/javascript">
                                                                $('#sample-data-table').DataTable({
                                                                    dom       : '<lf<t>ip>',
                                                                    "iDisplayLength": 100,
                                                                });
                                                            </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo URL;?>js/apps/e-commerce/products/products.js"></script>
                </div>
</div>


<script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
    <script type="text/javascript">
    $(document).ready(function (e) {
        $("#point_table a").addClass('active');
    });

     function dlt_app(point_table_id) {
        if (confirm("Are You Sure you want to delete?")) {
            $.ajax({
            type: 'POST',
            url: '<?php echo site_url('super_admin/point_table/delete_point_table');?>',
            data: {
                point_table_id:point_table_id
            },
            success: function(data) {
                var res = JSON.parse(data);
               if(res == 1){
                location.reload();
               }else{
                location.reload();
               }
             }
            });
        }
    }
</script>