<?php $db = mysqli_connect(HOST,USER,PASS,DB); ?>
<script> function account(id) { window.location = "<?php echo base_url();?>super_admin/matches/index/" + id; } </script>
<div class="content-wrapper">
                <nav id="toolbar" class="bg-white">
                    <div class="row no-gutters align-items-center flex-nowrap">
                        <div class="col">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row no-gutters align-items-center justify-content-end">
                                <button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="../images/avatars/profile.jpg">
                                        </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">
                    <div id="e-commerce-products" class="page-layout carded full-width">
                        <div class="top-bg bg-secondary"></div>
                        <div class="page-content-wrapper">
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">
                                <div class="col-12 col-sm">
                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="fa fa-2x fa-cubes"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4">Matches list</div>
                                            <div class="">Total Matches list: <?php echo count($records); ?></div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-auto search-wrapper px-2">
                                        <div class="input-group">
                                            <select class="form-control pull-left" style="margin: 0 2%;" onchange="account(this.value)">
                                                <option value="0" <?php if (isset($id) && $id == '0') echo "selected";?>>Pending</option>
                                                <option value="2"<?php if (isset($id) && $id == '2') echo "selected"; ?>>closed</option>
                                            </select>
                                            <a href="<?php echo base_url('super_admin/matches/add_matches'); ?>" class="btn btn-light justify-content-end">Add MATCHes</a>
                                        </div>
                                    </div>
                            </div>
                            <div class="page-content-card"> 
                                <div class="content custom-scrollbar">
                                  <table id="sample-data-table" class="table" style="font-size: 12px;">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Match Id</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Crickbuzz Match Number</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Teams 1</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Teams 2</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Match Type</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Match Time</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Action</span>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php foreach ($records as $r) { ?>
                                                                    <tr>
                                                                        <td><?php echo $r['matches_id']; ?></td>
                                                                        <td><?php echo $r['cb_match_number']; ?></td>
                                                                        <?php $g1 = mysqli_fetch_assoc(mysqli_query($db,"SELECT * FROM tbl_teams WHERE teams_id = '".$r['teams_id1']."'")); ?>
                                                                        <td><?php echo $g1['teams_name']; ?></td>
                                                                        <?php $g2 = mysqli_fetch_assoc(mysqli_query($db,"SELECT * FROM tbl_teams WHERE teams_id = '".$r['teams_id2']."'")); ?>
                                                                        <td><?php echo $g2['teams_name']; ?></td>
                                                                        <td><?php echo $r['match_type']; ?></td>
                                                                        <td><?php echo $r['match_time']; ?></td>
                                                                        <td style="width: 250px;">
                                                                            <a class="fa fa-pencil btn btn-primary" href="<?php echo base_url('super_admin/matches/add_matches') . "/" . $r['matches_id']; ?>" ></a>
                                                                            <a class="fa fa-trash btn btn-danger" onClick="dlt_app(<?php echo $r['matches_id']; ?>)" ></a>
                                                                            <?php if($r['status'] != 2){ ?>
                                                                                <button type="button" class="btn btn-secondary btn-fab close_matches_today" closematchesid=<?php echo $r['matches_id'];?> >
                                                                                        <i class="fa fa-2x fa-lock"></i>
                                                                                </button>
                                                                                <!-- <a class="" href="<?php echo base_url('super_admin/matches/close_matches') . "/" . $r['matches_id']; ?>"></a> -->
                                                                            <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>

                                                            <script type="text/javascript">
                                                                $('#sample-data-table').DataTable({
                                                                    dom       : '<lf<t>ip>',
                                                                    "iDisplayLength": 100,
                                                                });
                                                            </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo URL;?>js/apps/e-commerce/products/products.js"></script>
                </div>
</div>


<script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
    <script type="text/javascript">
    $(document).ready(function (e) {
        $("#matches a").addClass('active');
        $('.close_matches_today').on('click',function(){
            var matches_id = $(this).attr('closematchesid');
            if (confirm("Are You Sure you want to close this match?")) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url('super_admin/matches/close_matches');?>',
                    data: {
                        matches_id:matches_id
                    },
                    success: function(data) {
                        var res = JSON.parse(data);
                    if(res == 1){
                        location.reload();
                    }else{
                        location.reload();
                    }
                    }
                });
            }
        });

    });

     function dlt_app(matches_id) {
        if (confirm("Are You Sure you want to delete?")) {
            $.ajax({
            type: 'POST',
            url: '<?php echo site_url('super_admin/matches/delete_matches');?>',
            data: {
                matches_id:matches_id
            },
            success: function(data) {
                var res = JSON.parse(data);
               if(res == 1){
                location.reload();
               }else{
                location.reload();
               }
             }
            });
        }
    }
</script>