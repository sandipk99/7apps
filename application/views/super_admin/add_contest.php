<?php $db = mysqli_connect(HOST,USER,PASS,DB); ?>
<div class="content-wrapper">
                <nav id="toolbar" class="bg-white">
                    <div class="row no-gutters align-items-center flex-nowrap">
                        <div class="col">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row no-gutters align-items-center justify-content-end">
                                <button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="../images/avatars/profile.jpg">
                                        </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">
                    <div id="e-commerce-products" class="page-layout carded full-width">
                        <div class="top-bg bg-secondary"></div>
                        <div class="page-content-wrapper">
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">
                                <div class="col-12 col-sm">
                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="fa fa-2x fa-cubes"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4"> Add Contest Here</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="page-content-card "> 
                                  <div class="custom-scrollbar">  
                                    <div class="col-md-12 mt-5">
                                        <form role="form" action="" id="myForm" method="post" enctype="multipart/form-data">
                                        
                                        <div class="col-md-12">
                                            <div class="row">
                                                    <div class="col-md-6">
                                                            <!-- <div class="form-group">
                                                                <label for="exampleInputMatchId">Select Match</label>
                                                                <select  class="form-control" name="matches_id" id="exampleInputMatchId" required>
                                                                    <option>Select</option>
                                                                    <?php $sql = mysqli_query($db,"SELECT tbl_matches.*, (SELECT tbl_teams.teams_name FROM tbl_teams WHERE tbl_teams.teams_id = tbl_matches.teams_id1) AS teams_name1, (SELECT tbl_teams.teams_name FROM tbl_teams WHERE tbl_teams.teams_id = tbl_matches.teams_id2) AS teams_name2 FROM tbl_matches WHERE tbl_matches.status = 0"); while ($g1 = mysqli_fetch_assoc($sql)) { ?>
                                                                        <option value="<?php echo $g1['matches_id']?>"<?php if(isset($app_data)){ if($app_data[0]['matches_id'] == $g1['matches_id']){ echo "selected"; }} ?>><?php echo $g1['teams_name1'].' Vs '.$g1['teams_name2'].' ( '.$g1['match_type'].' ) - ( '.$g1['match_time'].' )';?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div> -->
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="users_teams" id="exampleInputUsersTeams" value="<?php if (isset($app_data)) {echo $app_data[0]['users_teams']; } ?>" class="form-control">
                                                            <label for="exampleInputUsersTeams">Total Teams :</label>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="entory_fee" id="exampleInputEntoryFees" value="<?php if (isset($app_data)) {echo $app_data[0]['entory_fee']; } ?>" class="form-control">
                                                            <label for="exampleInputEntoryFees">Entory Fee :</label>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="total_winners" id="exampleInputTotalWinners" value="<?php if (isset($app_data)) {echo $app_data[0]['total_winners']; } ?>" class="form-control">
                                                            <label for="exampleInputTotalWinners">Total Winners :</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="total_winnings_price" id="exampleInputTotalWinningsPrice" value="<?php if (isset($app_data)) {echo $app_data[0]['total_winnings_price']; } ?>" class="form-control">
                                                            <label for="exampleInputTotalWinningsPrice">Total Winning Price :</label>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                                <div class="col-md-12" style="margin-top: 60px;">
                                                    <div class="row">       
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList1" value="1" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList1">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices1" value="<?php if (isset($app_data11)) {echo $app_data11[0]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices1">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList2" value="2" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList2">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices2" value="<?php if (isset($app_data11)) {echo $app_data11[1]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices2">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList3" value="3" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList3">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices3" value="<?php if (isset($app_data11)) {echo $app_data11[2]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices3">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList4" value="4" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList4">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices4" value="<?php if (isset($app_data11)) {echo $app_data11[3]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices4">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList5" value="5" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList5">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices5" value="<?php if (isset($app_data11)) {echo $app_data11[4]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices5">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList6" value="6" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList6">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices6" value="<?php if (isset($app_data11)) {echo $app_data11[5]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices6">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList7" value="7" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList7">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices7" value="<?php if (isset($app_data11)) {echo $app_data11[6]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices7">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList8" value="8" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList8">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices8" value="<?php if (isset($app_data11)) {echo $app_data11[7]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices8">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList9" value="9" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList9">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices9" value="<?php if (isset($app_data11)) {echo $app_data11[8]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices9">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList10" value="10" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList10">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices10" value="<?php if (isset($app_data11)) {echo $app_data11[9]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices10">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList11" value="11" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList11">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices11" value="<?php if (isset($app_data11)) {echo $app_data11[10]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices11">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList12" value="12" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList12">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices12" value="<?php if (isset($app_data11)) {echo $app_data11[11]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices12">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList13" value="13" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList13">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices13" value="<?php if (isset($app_data11)) {echo $app_data11[12]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices13">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList14" value="14" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList14">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices14" value="<?php if (isset($app_data11)) {echo $app_data11[13]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices14">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList15" value="15" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList15">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices15" value="<?php if (isset($app_data11)) {echo $app_data11[14]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices15">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList16" value="16" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList16">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices16" value="<?php if (isset($app_data11)) {echo $app_data11[15]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices16">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList17" value="17" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList17">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices17" value="<?php if (isset($app_data11)) {echo $app_data11[16]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices17">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList18" value="18" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList18">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices18" value="<?php if (isset($app_data11)) {echo $app_data11[17]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices18">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList19" value="19" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList19">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices19" value="<?php if (isset($app_data11)) {echo $app_data11[18]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices19">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList20" value="20" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList20">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices20" value="<?php if (isset($app_data11)) {echo $app_data11[19]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices20">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList21" value="21" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList21">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices21" value="<?php if (isset($app_data11)) {echo $app_data11[20]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices21">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList22" value="22" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList22">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices22" value="<?php if (isset($app_data11)) {echo $app_data11[21]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices22">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList23" value="23" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList23">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices23" value="<?php if (isset($app_data11)) {echo $app_data11[22]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices23">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList24" value="24" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList24">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices24" value="<?php if (isset($app_data11)) {echo $app_data11[23]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices24">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList25" value="25" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList25">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices25" value="<?php if (isset($app_data11)) {echo $app_data11[24]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices25">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList26" value="26" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList26">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices26" value="<?php if (isset($app_data11)) {echo $app_data11[25]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices26">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList27" value="27" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList27">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices27" value="<?php if (isset($app_data11)) {echo $app_data11[26]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices27">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList28" value="28" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList28">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices28" value="<?php if (isset($app_data11)) {echo $app_data11[27]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices28">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList29" value="29" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList29">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices29" value="<?php if (isset($app_data11)) {echo $app_data11[28]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices29">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList30" value="30" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList30">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices30" value="<?php if (isset($app_data11)) {echo $app_data11[29]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices30">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList31" value="31" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList31">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices31" value="<?php if (isset($app_data11)) {echo $app_data11[30]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices31">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList32" value="32" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList32">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices32" value="<?php if (isset($app_data11)) {echo $app_data11[31]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices32">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList33" value="33" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList33">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices33" value="<?php if (isset($app_data11)) {echo $app_data11[32]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices33">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList34" value="34" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList34">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices34" value="<?php if (isset($app_data11)) {echo $app_data11[33]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices34">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList35" value="35" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList35">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices35" value="<?php if (isset($app_data11)) {echo $app_data11[34]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices35">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList36" value="36" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList36">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices36" value="<?php if (isset($app_data11)) {echo $app_data11[35]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices36">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList37" value="37" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList37">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices37" value="<?php if (isset($app_data11)) {echo $app_data11[36]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices37">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList38" value="38" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList38">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices38" value="<?php if (isset($app_data11)) {echo $app_data11[37]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices38">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList39" value="39" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList39">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices39" value="<?php if (isset($app_data11)) {echo $app_data11[38]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices39">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList40" value="40" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList40">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices40" value="<?php if (isset($app_data11)) {echo $app_data11[39]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices40">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList41" value="41" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList41">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices41" value="<?php if (isset($app_data11)) {echo $app_data11[40]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices41">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList42" value="42" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList42">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices42" value="<?php if (isset($app_data11)) {echo $app_data11[41]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices42">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList43" value="43" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList43">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices43" value="<?php if (isset($app_data11)) {echo $app_data11[42]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices43">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList44" value="44" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList44">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices44" value="<?php if (isset($app_data11)) {echo $app_data11[43]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices44">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList45" value="45" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList45">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices45" value="<?php if (isset($app_data11)) {echo $app_data11[44]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices45">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList46" value="46" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList46">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices46" value="<?php if (isset($app_data11)) {echo $app_data11[45]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices46">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList47" value="47" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList47">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices47" value="<?php if (isset($app_data11)) {echo $app_data11[46]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices47">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList48" value="48" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList48">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices48" value="<?php if (isset($app_data11)) {echo $app_data11[47]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices48">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">       
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList49" value="49" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList49">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices49" value="<?php if (isset($app_data11)) {echo $app_data11[48]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices49">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_list[]" id="exampleInputWinnerList50" value="50" class="form-control" readonly>
                                                                <label for="exampleInputWinnerList50">Winner Number :</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="text" name="contest_price[]" id="exampleInputContestPrices50" value="<?php if (isset($app_data11)) {echo $app_data11[49]['contest_price']; } ?>" class="form-control exampleInputContestPrices">
                                                                <label for="exampleInputContestPrices50">Contest Price :</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">   
                                                    <div class="form-group">                               
                                                        <button type="button" name="back" onClick="javascript:history.go(-1);" class="btn btn-primary"><i 
                                                                class="fa fa-2x fa-chevron-circle-left"> &nbsp;</i>
                                                        </button>
                                                        <button type="submit" name="submit" class="btn btn-secondary">Submit</button>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                  </div>                                          
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo URL;?>js/apps/e-commerce/products/products.js"></script>
                </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $("#contest_list a").addClass('active');
        
        $('.exampleInputContestPrices').on('change',function(){
            var exampleInputTotalWinners = $('#exampleInputTotalWinners').val();
            var exampleInputTotalWinningsPrice = $('#exampleInputTotalWinningsPrice').val();
            var valueArray = $('.exampleInputContestPrices').map(function() {
                            return this.value;
                        }).get();
            var total = 0;
            for(var i=0;i<exampleInputTotalWinners;i++){
                if(valueArray[i] != ''){
                    total = parseInt(total) + parseInt(valueArray[i]);
                }
            }
            if(total != exampleInputTotalWinningsPrice){
                alert('Total Contest Winner : '+ exampleInputTotalWinners+' ,Total Winning Price : '+exampleInputTotalWinningsPrice);
            }
        });
    });

    function image_readURL(input) {
        if (input.files && input.files[0]){
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#image').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>