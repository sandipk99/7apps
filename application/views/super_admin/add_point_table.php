<?php $db = mysqli_connect(HOST,USER,PASS,DB); ?>
<div class="content-wrapper">
                <nav id="toolbar" class="bg-white">
                    <div class="row no-gutters align-items-center flex-nowrap">
                        <div class="col">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row no-gutters align-items-center justify-content-end">
                                <button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="../images/avatars/profile.jpg">
                                        </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">
                    <div id="e-commerce-products" class="page-layout carded full-width">
                        <div class="top-bg bg-secondary"></div>
                        <div class="page-content-wrapper">
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">
                                <div class="col-12 col-sm">
                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="fa fa-2x fa-cubes"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4"> Add Point Table Here</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="page-content-card "> 
                                  <div class="custom-scrollbar">  
                                    <div class="col-md-12 mt-5">
                                        <form role="form" action="" id="myForm" method="post" enctype="multipart/form-data">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="point_table_text" id="exampleInputPointTableText" value="<?php if (isset($app_data)) {echo $app_data[0]['point_table_text']; } ?>" class="form-control">
                                                            <label for="exampleInputPointTableText">Text :</label>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="point_table_key" id="exampleInputPointTableKey" value="<?php if (isset($app_data)) {echo $app_data[0]['point_table_key']; } ?>" class="form-control">
                                                            <label for="exampleInputPointTableKey">Point Table Key :</label>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="point_table_value" id="exampleInputPointTableValue" value="<?php if (isset($app_data)) {echo $app_data[0]['point_table_value']; } ?>" class="form-control">
                                                            <label for="exampleInputPointTableValue">Point Table Value :</label>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                 <div class="row">       
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="point_table_pm" id="exampleInputPointTablePM" value="<?php if (isset($app_data)) {echo $app_data[0]['point_table_pm']; } ?>" class="form-control">
                                                            <label for="exampleInputPointTablePM">+ / - :</label>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                                <div class="col-md-12">   
                                                    <div class="form-group">                               
                                                        <button type="button" name="back" onClick="javascript:history.go(-1);" class="btn btn-primary">
                                                            <i class="fa fa-2x fa-chevron-circle-left"> &nbsp;</i>
                                                        </button>
                                                        <button type="submit" name="submit" class="btn btn-secondary">Submit</button>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                  </div>                                          
                                   

                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo URL;?>js/apps/e-commerce/products/products.js"></script>
                </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $("#point_table a").addClass('active');
    });
</script>