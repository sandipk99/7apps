<?php $db = mysqli_connect(HOST,USER,PASS,DB); ?>
<script> function account(id) { window.location = "<?php echo base_url();?>super_admin/recharge/index/" + id; } </script>
<div class="content-wrapper">
                <nav id="toolbar" class="bg-white">
                    <div class="row no-gutters align-items-center flex-nowrap">
                        <div class="col">
                            <div class="row no-gutters align-items-center flex-nowrap">
                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="row no-gutters align-items-center justify-content-end">
                                <button type="button" class="quick-panel-button btn btn-icon" data-fuse-bar-toggle="quick-panel-sidebar">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="../images/avatars/profile.jpg">
                                        </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">
                    <div id="e-commerce-products" class="page-layout carded full-width">
                        <div class="top-bg bg-secondary"></div>
                        <div class="page-content-wrapper">
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">
                                <div class="col-12 col-sm">
                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="fa fa-2x fa-cubes"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4">Playes list</div>
                                            <div class="">Total Playes list: <?php echo count($records); ?></div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-auto search-wrapper px-2">
                                        <div class="input-group">
                                            <a href="<?php echo base_url('super_admin/players/add_players'); ?>" class="btn btn-light justify-content-end">Add PLAYES</a>
                                        </div>
                                    </div>
                            </div>
                            <div class="page-content-card"> 
                                <div class="content custom-scrollbar">
                                  <table id="sample-data-table" class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Playes Id</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Position</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Icon</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Players Name</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Teams</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Credits</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">WK</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">BAT</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">AR</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">BOWL</span>
                                                                            </div>
                                                                        </th>
                                                                        <th class="secondary-text">
                                                                            <div class="table-header">
                                                                                <span class="column-title">Status</span>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="sortable">
                                                                <?php foreach ($records as $r) { ?>
                                                                    <tr id="<?php echo $r['players_id']; ?>">

                                                                        <td><span ><?php echo $r['players_id']; ?></span></td>
                                                                        <td><?php echo $r['position']; ?></td>
                                                                        <td><?php if($r['icon']){ ?><img src="<?php echo IMAGE.'players_icon/'.$r['icon']; ?>" height="50px" width="50px" style="border-radius: 10px"><?php } else { ?><img src="<?php echo IMAGE.'no_image.png'; ?>" height="50px" width="50px" style="border-radius: 10px"><?php } ?></td>
                                                                        <td><?php echo $r['players_name']; ?></td>
                                                                        <?php $g = mysqli_fetch_assoc(mysqli_query($db,"SELECT * FROM tbl_teams WHERE teams_id = '".$r['teams_id']."'")); ?>
                                                                        <td><?php echo $g['teams_name']; ?></td>
                                                                        <td><?php echo $r['players_credits']; ?></td>
                                                                        <td><input type="checkbox" playersid="<?php echo $r['players_id']; ?>" class="inlineCheckboxWK" <?php echo ($r['WK']== '1' ? 'checked="true"' : '');?> ></td>
                                                                        <td><input type="checkbox" playersid="<?php echo $r['players_id']; ?>" class="inlineCheckboxBAT"  <?php echo ($r['BAT']=='1' ? 'checked="true"' : '');?> ></td>
                                                                        <td><input type="checkbox" playersid="<?php echo $r['players_id']; ?>" class="inlineCheckboxAR" <?php echo ($r['AR']=='1' ? 'checked="true"' : '');?> ></td>
                                                                        <td><input type="checkbox" playersid="<?php echo $r['players_id']; ?>" class="inlineCheckboxBOWL" <?php echo ($r['BOWL']=='1' ? 'checked="true"' : '');?> ></td>
                                                                        <td>
                                                                            <a class="fa fa-pencil btn btn-primary" href="<?php echo base_url('super_admin/players/add_players') . "/" . $r['players_id']; ?>" style="margin-bottom: 3px"></a>
                                                                            <a class="fa fa-trash btn btn-danger" onClick="dlt_app(<?php echo $r['players_id']; ?>)" style="margin-bottom: 3px"></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                

                                                                </tbody>
                                                            </table>

                                                            <script type="text/javascript">
                                                                $('#sample-data-table').DataTable({
                                                                    dom       : '<lf<t>ip>',
                                                                    "iDisplayLength": 1000,
                                                                    "order": [[ 1, "DESC" ]]
                                                                });
                                                            </script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo URL;?>js/apps/e-commerce/products/products.js"></script>
                </div>
</div>

<script>
    $(function () {
        $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
    <script type="text/javascript">
    $(document).ready(function (e) {
        $("#players a").addClass('active');

        $('#sortable').sortable({
            axis: 'y',
            opacity: 0.7,
            handle: 'span',
            update: function(event, ui) {
                var list_sortable = $(this).sortable('toArray').toString();
                $.ajax({
                    url: '<?php echo site_url('super_admin/players/update_players_position');?>',
                    type: 'POST',
                    data: {list_order:list_sortable},
                    success: function(data) {
                        //finished
                    }
                });
            }
        });   
        $(document).on('click','.inlineCheckboxWK',function(){
            if($(this).prop('checked')){
                var WK = '1';
            }else{
                var WK = '0';
            }
            var players_id = $(this).attr('playersid');
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('super_admin/players/update_players_status_WK');?>',
                data: {
                    players_id:players_id,
                    WK:WK
                },
                success: function(data) {

                }
            });
        });
        $(document).on('click','.inlineCheckboxBAT',function(){
            if($(this).prop('checked')){
                var BAT = '1';
            }else{
                var BAT = '0';
            }
            var players_id = $(this).attr('playersid');
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('super_admin/players/update_players_status_BAT');?>',
                data: {
                    players_id:players_id,
                    BAT:BAT
                },
                success: function(data) {

                }
            });
        });
        $(document).on('click','.inlineCheckboxAR',function(){
            if($(this).prop('checked')){
                var AR = '1';
            }else{
                var AR = '0';
            }
            var players_id = $(this).attr('playersid');
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('super_admin/players/update_players_status_AR');?>',
                data: {
                    players_id:players_id,
                    AR:AR
                },
                success: function(data) {

                }
            });
        });
        $(document).on('click','.inlineCheckboxBOWL',function(){
            if($(this).prop('checked')){
                var BOWl = '1';
            }else{
                var BOWl = '0';
            }
            var players_id = $(this).attr('playersid');
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('super_admin/players/update_players_status_BOWl');?>',
                data: {
                    players_id:players_id,
                    BOWl:BOWl
                },
                success: function(data) {

                }
            });

        });
        
        

    });

     function dlt_app(players_id) {
        if (confirm("Are You Sure you want to delete?")) {
            $.ajax({
            type: 'POST',
            url: '<?php echo site_url('super_admin/players/delete_players');?>',
            data: {
                players_id:players_id
            },
            success: function(data) {
                var res = JSON.parse(data);
               if(res == 1){
                location.reload();
               }else{
                location.reload();
               }
             }
            });
        }
    }
</script>