<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Paytm extends CI_Controller
{
	public $response;
	public function __construct()
	{
        parent::__construct();
        $this->load->library('encdec_paytm_get');
        $this->load->model('query_model', 'qm', TRUE);
		$this->response = [
			'status' => '0',
			'message' => "Error!"
		];
		header('Content-Type: application/json');
	}

    public function get_check_sum()
    {
        
        $user_id = $_POST['user_id'];
        $unique_id = $_POST['unique_id'];
        $contest_id = $_POST['contest_id'];
        $matches_id = $_POST['matches_id'];
        $mobile_no = $_POST['mobile_no'];
        $email = $_POST['email'];
        if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
            if($contest_id != '' && $matches_id != ''){
                $contest_participate_list = $this->qm->select_where('tbl_contest_participate',array('user_id'=>$user_id,'contest_id'=>$contest_id,'matches_id'=>$matches_id));
                if(isset($contest_participate_list[0])){
                    $payments_list = $this->qm->select_where('tbl_payments',array('user_id'=>$user_id,'contest_id'=>$contest_id,'matches_id'=>$matches_id));
                    if(count($payments_list) > 0){
                        $this->response = [
                            'status'=>0,
                            'data'=>'Error!'
                        ];
                    }else{
                        $contest_list = $this->qm->select_where_row('tbl_contest',array('contest_id'=>$contest_id));
                        $order_id = "ORDERS-".rand('00000','99999').rand('00000','99999').rand('00000','99999').'-'.$contest_participate_list[0]['contest_participate_id'];
                        $checkSum = "";
                        $paramList = array();
                        $paramList["REQUEST_TYPE"] = 'DEFAULT';
                        $paramList["MID"] = 'MAHADE69070531790825';
                        $paramList["ORDER_ID"] = $order_id;
                        $paramList["CUST_ID"] = $user_id;
                        $paramList["TXN_AMOUNT"] = $contest_list['entory_fee'];
                        $paramList["CHANNEL_ID"] = 'WAP'; 
                        $paramList["INDUSTRY_TYPE_ID"] = 'Retail';
                        $paramList["WEBSITE"] = 'APP_STAGING';
                        $paramList["MOBILE_NO"] = '7777777777';//$mobile_no;
                        $paramList["EMAIL"] = $email;
                        $paramList["CALLBACK_URL"] = 'http://phpstack-165502-477873.cloudwaysapps.com/paytm/buys';
                        $checkSum = $this->encdec_paytm_get->getChecksumFromArray($paramList, PAYTM_MERCHANT_KEY_GET);
                        $paramList["CHECKSUMHASH"] = $checkSum;
                        $this->response = [
                            'status'=>1,
                            'data'=>[
                                'param'=>$paramList
                            ]
                        ];
                    }
                }else{
                    $this->response = [
                        'status'=>0,
                        'data'=>'Error!'
                    ];
                }
            }else{
                $this->response = [
                    'status'=>0,
                    'data'=>'Error!'
                ];
            }
        }else{
            $this->response = [
                'status'=>0,
                'data'=>'Error!'
            ];
        }
        echo json_encode($this->response);
    }

} 