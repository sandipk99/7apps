<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Paypal extends CI_Controller 
    {
         function  __construct(){
            parent::__construct();
            $this->load->library('paypal_lib');
            $this->load->model('product');
            $this->load->model('query_model', 'qm', TRUE);
         }
         
         function success(){
            $paypalInfo = $this->input->get();
            $data['item_number'] = isset($paypalInfo['item_number']) ? $paypalInfo['item_number'] : ''; 
            $data['txn_id'] = isset($paypalInfo["tx"]) ? $paypalInfo["tx"] : '';
            $data['payment_amt'] = isset($paypalInfo["amt"]) ? $paypalInfo["amt"] : '';
            $data['currency_code'] = isset($paypalInfo["cc"]) ? $paypalInfo["cc"] : '';
            $data['status'] = isset($paypalInfo["st"]) ? $paypalInfo["st"] : '';
            $this->load->view('paypal/success', $data);
         }
         
         function cancel(){
            $this->load->view('paypal/cancel');
         }
         
         function ipn(){
            $paypalInfo = $this->input->post();
            $data['user_id']        = $paypalInfo['custom'];
            $data['product_id']     = $paypalInfo["item_number"];
            $item_number = explode('-', $paypalInfo["item_number"]);
            $data['contest_id']     = $item_number[1];
            $data['matches_id']     = $item_number[2];
            $data['txn_id']         = $paypalInfo["txn_id"];
            $data['payment_gross']  = $paypalInfo["payment_gross"];
            $data['currency_code']  = $paypalInfo["mc_currency"];
            $data['payer_email']    = $paypalInfo["payer_email"];
            $data['payment_status'] = $paypalInfo["payment_status"];
            $data['created_date']   = date('Y-m-d H:i:s');

            $data1['user_id']        = $paypalInfo['custom'];
            $data1['txn_type']       =   'credit';
            $data1['paypal_txn_id']  = $paypalInfo["txn_id"];
            $data1['payment_gross']  = $paypalInfo["payment_gross"];
            $data1['currency_code']  = $paypalInfo["mc_currency"];
            $data1['payer_email']    = $paypalInfo["payer_email"];
            $data1['payment_status'] = $paypalInfo["payment_status"];
            $data1['created_date']   = date('Y-m-d H:i:s');
            
            $paypalURL = $this->paypal_lib->paypal_url;        
            $result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
            
            if(preg_match("/VERIFIED/i",$result)){
                $this->product->insertTransaction($data);
                $this->product->insertTransaction123($data1);
                $this->add_contest_participation_group($data);
            }
        }
        
        function add_contest_participation_group($data)
        {
            $user_id = $data['user_id'];
            $contest_id = $data['contest_id'];
            $matches_id = $data['matches_id'];
            $contest_participate_list = $this->qm->select_where('tbl_contest_participate',array('user_id'=>$user_id,'contest_id'=>$contest_id,'matches_id'=>$matches_id));
            $contest_list = $this->qm->select_where_row('tbl_contest',array('contest_id'=>$contest_id));
            if(count($contest_list)>0){
                if(count($contest_participate_list[0])>0){
                    $players_group_list = $this->qm->select_where('tbl_players_group_list',array('contest_id'=>$contest_id,'matches_id'=>$matches_id));                   
                    if(count($players_group_list)>0){
                        $players_group_listssss = explode(',',$players_group_list[count($players_group_list)-1]['contest_participate_list']);
                        if(count($players_group_listssss) < $contest_list['users_teams']){
                            array_push($players_group_listssss,$contest_participate_list[0]['contest_participate_id']);
                            $players_group_listss = implode(',',$players_group_listssss);
                            $post_data22 = array(
                                'contest_participate_list' => $players_group_listss,
                            );
                            $where = array('players_group_id' => $players_group_list[count($players_group_list)-1]['players_group_id']);
                            $this->qm->updt('tbl_players_group_list', $post_data22, $where);
                            return true;
                        }else{
                            $post_data13 = array(
                                'contest_id' => $contest_id,
                                'matches_id' => $matches_id,
                                'contest_teams'=>$contest_list['users_teams'],
                                'contest_participate_list' => $contest_participate_list[0]['contest_participate_id'],
                            );
                            $this->qm->ins('tbl_players_group_list', $post_data13);
                            return true;
                        }
                    }else{
                        $post_data21 = array(
                            'contest_id' => $contest_id,
                            'matches_id' => $matches_id,
                            'contest_teams'=>$contest_list['users_teams'],
                            'contest_participate_list' => $contest_participate_list[0]['contest_participate_id'],
                        );
                        $this->qm->ins('tbl_players_group_list', $post_data21);
                        return true;
                    }
                }
            }
        }

        function buy(){
            $user_id = $this->uri->segment(3);
            $contest_id = $this->uri->segment(4);
            $matches_id = $this->uri->segment(5);
            $contest_participate_list = $this->qm->select_where('tbl_contest_participate',array('user_id'=>$user_id,'contest_id'=>$contest_id,'matches_id'=>$matches_id));
            if(isset($contest_participate_list[0])){
                $payments_list = $this->qm->select_where('tbl_payments',array('user_id'=>$user_id,'contest_id'=>$contest_id,'matches_id'=>$matches_id));
                if(count($payments_list) > 0){
                    $this->load->view('paypal/alredy_done');
                }else{
                    $contest_list = $this->qm->select_where_row('tbl_contest',array('contest_id'=>$contest_id));
                    $returnURL = URL.'Paypal/success';
                    $cancelURL = URL.'Paypal/cancel';
                    $notifyURL = URL.'Paypal/ipn';
                    $userID = $user_id.'-'.$contest_id.'-'.$matches_id;
                    $logo = URL.'images/profile.png';
                    $this->paypal_lib->add_field('return', $returnURL);
                    $this->paypal_lib->add_field('cancel_return', $cancelURL);
                    $this->paypal_lib->add_field('notify_url', $notifyURL);
                    $this->paypal_lib->add_field('item_name', 'Contest');
                    $this->paypal_lib->add_field('custom', $user_id);
                    $this->paypal_lib->add_field('item_number', $userID);
                    $this->paypal_lib->add_field('amount',  $contest_list['entory_fee']);        
                    $this->paypal_lib->image($logo);
                    $this->paypal_lib->paypal_auto_form();
                }
            }else{
                $this->load->view('paypal/cancel');
            }
        }

        function c_amt()
        {
            $userID = $this->uri->segment(3);
            $amount = $this->uri->segment(4);
            $returnURL = URL.'Paypal/success';
            $cancelURL = URL.'Paypal/cancel';
            $notifyURL = URL.'Paypal/ipnn';
            $logo = URL.'images/profile.png';
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            $this->paypal_lib->add_field('item_name', 'Credit');
            $this->paypal_lib->add_field('custom', $userID);
            $this->paypal_lib->add_field('item_number', $userID);
            $this->paypal_lib->add_field('amount',  $amount);        
            $this->paypal_lib->image($logo);
            $this->paypal_lib->paypal_auto_form();
        }
        function ipnn(){
            $paypalInfo = $this->input->post();
            $data['user_id']        = $paypalInfo['custom'];
            $data['txn_type']       =   'credit';
            $data['paypal_txn_id']  = $paypalInfo["txn_id"];
            $data['payment_gross']  = $paypalInfo["payment_gross"];
            $data['currency_code']  = $paypalInfo["mc_currency"];
            $data['payer_email']    = $paypalInfo["payer_email"];
            $data['payment_status'] = $paypalInfo["payment_status"];
            $data['created_date']   = date('Y-m-d H:i:s');
            $paypalURL = $this->paypal_lib->paypal_url;        
            $result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
            if(preg_match("/VERIFIED/i",$result)){
                $this->product->insertTransaction123($data);
                $this->qm->increment('tbl_users', array('user_id' => $data['user_id']), $data['payment_gross'], 'balance');
            }
        }

    }
?>