<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Service extends CI_Controller
{
	public $response;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('query_model', 'qm', TRUE);
		
		$this->load->library('session');
		$this->load->library('my_phpmailer');
		$this->load->library("simple_html_dom");

		$this->response = [
			'status' => '0',
			'message' => "Error!"
		];
		header('Content-Type: application/json');
		
	}
	public function check_referral()
	{
		$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		if(stripos($ua,'android') !== false) {
			$referral = $_REQUEST['referral'];
			if((($this->qm->num_where_row('tbl_users',['referral_key'=>$referral])) > 0) OR ($referral == '0123456789')){
				$this->response = [
					'status' => "1",
					'message' => "Success!"
				];
			}else{
				$this->response = [
					'status' => "0",
					'message' => "Invalid referrel code!"
				];
			}
			echo json_encode($this->response);	
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
			echo json_encode($this->response);
		}
		
	}
	public function getcapcha()
	{
			$chars_a = "0123456789abcdefghijklmnopqrstuvwxyz";
			$text = "";
			for ($i = 0; $i < 5; $i++) {
				$text .= $chars_a[mt_rand(0, strlen($chars_a) - 1)];
			}		  
			$text2 = rand('00000','99999').'-'.rand('00000','99999');
			$location = 'images/capcha/'.$text2.'.png';
			$session_id = $this->randomString_session();
			$data = [
				'session_id'=>$session_id,
				'images'=>$location,
				'capcha'=>$text
			];
			$this->qm->ins('tbl_user_token',$data);

			$image = imagecreate( 100, 50 );                         
			$background  = imagecolorallocate( $image, 255,255,255);
			$text_colour = imagecolorallocate( $image, 0, 0, 0 );
			imagestring( $image, 4, 30, 15, $text, $text_colour );
			imagesetthickness ( $image, 5 );
			imagepng($image,$location);
			imagecolordeallocate( $image,$text_colour );
			imagecolordeallocate($image, $background );
			imagedestroy( $image );	
			$this->response =[
				'status'=>'1',
				'location'=>URL.$location,
				'session_id'=>$session_id
			];
		echo json_encode($this->response);
	}
	
	public function user_verifylogin1()
	{
		$referral = $_POST['referral'] != '' ? $_POST['referral'] :'01234546789';
		$email = $_POST['email'];
		$mobile = $_POST['mobile'];
		$password = $_POST['password'];
		$username = ucfirst($_POST['username']);
		$imei = $_POST['imei'];
		$token = $_POST['token'];
		$session_id = $_POST['session_id'];
		$capcha = $_POST['capcha'];

		$unique_id = $this->randomString_unique_id();
		$xsxsxs = $this->db->query("SELECT * FROM tbl_user_token WHERE session_id='$session_id'")->row();
		if (($this->is_imei($imei)) == 1) {
			if($xsxsxs->session_id == $session_id && $xsxsxs->capcha == $capcha){
					$path = $xsxsxs->images;
					if (file_exists($path)) {
						unlink($path);
					}	
					$this->db->query("DELETE FROM tbl_user_token WHERE session_id='$session_id'");
					$users = $this->qm->select_where_row('tbl_users', ['email' => $email]);
					if ($users['status'] == 1) {
						if(password_verify($password, $users['password'])){
								$this->response = [
									'status' => "1",
									'message' => "Login successful",
									'IMAGE_URL' => IMAGE . 'user_profile/',
									'user_data' => [
													'user_id'=>$users['user_id'],
													'referral_key'=>$users['referral_key'],
													'username'=>$users['username'],
													'email'=>$users['email'],
													'mobile'=>$users['mobile'],
													'referral_count'=>$users['referral_count'],
													'balance'=>$users['balance'],
													'referral_balance'=>$users['referral_balance'],
													'status'=>$users['status'],
													'unique_id'=>$users['unique_id'],
													'favorite_team'=>$users['favorite_team'],
													'dateofbirth'=>$users['dateofbirth'],
													'gender'=>$users['gender'],
													'address'=>$users['address'],
													'city'=>$users['city'],
													'pincode'=>$users['pincode'],
													'state'=>$users['state'],
													'country'=>$users['country'],
													'user_profile'=>$users['user_profile']
												]
								];
							} else {
								$this->response = [
									'status' => '0',
									'message' => "Error! Email or password incorrect"
								];
							}
					}else{
						if($token == '' && $mobile == ''){
							$this->response = [
								'status' => '0',
								'message' => "Error! Please register"
							];
						}else{
							$data = [
								'referral_key'=>$this->randomString_refferal(),
								'referral' => $referral,
								'email' => $email,
								'password' => password_hash($password, PASSWORD_DEFAULT) ,
								'mobile' => $mobile,
								'username' => $username,
								'imei' => $imei,
								'token' => $token,
								'unique_id' => $unique_id,
								'email_verify_token' => $this->randomString_session(),
								'email_verify_token_status' => '0',
								'create_date' => date('Y-m-d H:i:s')
							];
							$insert_id = $this->qm->ins('tbl_users', $data);
							if ($insert_id) {
								$refUser = $this->qm->select_where_row('tbl_users', ['referral_key' => $referral]);
								if (!empty($refUser)) {
									$this->qm->increment('tbl_users', array('user_id' => $refUser['user_id']), '1', 'referral_count');
								}
								$this->response = [
									'status' => "1",
									'message' => "SignUp successful"
								];
							}else{
								$this->response = [
									'status' => "0",
									'message' => "Error"
								];
							}
						}
					}
			}else{
				$path = $xsxsxs->images;
				if (file_exists($path)) {
					unlink($path);
					$this->db->query("DELETE FROM tbl_user_token WHERE session_id='$session_id'");
					$this->response = [
									'status'=>'5',
									'message'=>'error! '
							];
				}else{
					$this->response = [
						'status'=>'5',
						'message'=>'error! '
					];
				}
			}
		} else {
					$path = $xsxsxs->images;
					if (file_exists($path)) {
						unlink($path);
						$this->db->query("DELETE FROM tbl_user_token WHERE session_id='$session_id'");
						$this->response = [
							'status' => '0',
							'message' =>'Your devices not verify!',
						];
					}else{
						$this->response = [
							'status' => '0',
							'message' =>'Your devices not verify!',
						];
					}
		}
		echo json_encode($this->response);
	}

	public function update_user_profile()
	{
		$user_id = $_REQUEST['user_id'];
		$unique_id = $_POST['unique_id'];
		$favorite_team = $_POST['favorite_team'];
		$dateofbirth = $_POST['dateofbirth'];
		$gender = $_POST['gender'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$pincode = $_POST['pincode'];
		$state = $_POST['state'];
		$country = $_POST['country'];
		$mobile = $_POST['mobile'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$data['favorite_team'] = $favorite_team;
			$data['dateofbirth'] = $dateofbirth;
			$data['gender'] = $gender;
			$data['address'] = $address;
			$data['city'] = $city;
			$data['pincode'] = $pincode;
			$data['state'] = $state;
			$data['country'] = $country;
			$data['mobile'] = $mobile;
			$data['updated_date'] = date('Y-m-d H:i:s');
			if (isset($_POST['user_profile']) && ($_POST['user_profile']) != "") {
				$barcode = $_POST['user_profile'];
				$binary=base64_decode($barcode);
				header('Content-Type: bitmap; charset=utf-8');
				$file_name = rand(1111111, 9999999).'.png';
				$file = fopen('./images/user_profile/'.$file_name, 'wb');
				fwrite($file, $binary);
				fclose($file);
				$data['user_profile'] = $file_name;
			}
			$this->qm->updt('tbl_users',$data,['user_id'=>$user_id]);
			$users = $this->qm->select_where_row('tbl_users', ['user_id'=>$user_id]);
			$this->response = [
				'status' => "1",
				'message' => "Login successful",
				'IMAGE_URL' => IMAGE . 'user_profile/',
				'user_data' => [
							'user_id'=>$users['user_id'],
							'referral_key'=>$users['referral_key'],
							'username'=>$users['username'],
							'email'=>$users['email'],
							'mobile'=>$users['mobile'],
							'referral_count'=>$users['referral_count'],
							'balance'=>$users['balance'],
							'referral_balance'=>$users['referral_balance'],
							'status'=>$users['status'],
							'unique_id'=>$users['unique_id'],
							'favorite_team'=>$users['favorite_team'],
							'dateofbirth'=>$users['dateofbirth'],
							'gender'=>$users['gender'],
							'address'=>$users['address'],
							'city'=>$users['city'],
							'pincode'=>$users['pincode'],
							'state'=>$users['state'],
							'country'=>$users['country'],
							'user_profile'=>$users['user_profile']
							]
				];

		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);

	}

	public function user_forgotpassword()
	{
		$user_id = $_REQUEST['user_id'];
		$unique_id = $_POST['unique_id'];
		$email = $_POST['email'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$users = $this->qm->select_where_row('tbl_users', ['email' => $email]);
			if($users['status'] == 1){
					$password = $this->randomString_refferal();
					$new_pass_date = date('Y-m-d H:i:s');
					$password_hash = password_hash($password, PASSWORD_DEFAULT);
					$this->qm->updt('tbl_users', ['password' => $password_hash,'new_pass_date'=>$new_pass_date], ['email' => $email]);
					if($this->send_mail($email,$password)){
						$this->response = [
							'status' => '1',
							'message' =>'We sent new password on your email!',
						];
					}else{
						$this->response = [
							'status' => '0',
							'message' =>'Error!',
						];
					}
			}else{
				$this->response = [
					'status' => '0',
					'message' =>'User Not Available!',
				];
			}
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);
	}

	public function user_changepassword()
	{
		$user_id = $_REQUEST['user_id'];
		$unique_id = $_POST['unique_id'];
		$old_password = $_POST['old_password'];
		$new_password = $_POST['new_password'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$users = $this->qm->select_where_row('tbl_users', ['user_id' => $user_id]);
			if($users['status'] == 1){
				if(password_verify($old_password, $users['password'])){
					$updated_date = date('Y-m-d H:i:s');
					$password_hash = password_hash($new_password, PASSWORD_DEFAULT);
					$this->qm->updt('tbl_users', ['password' => $password_hash,'updated_date'=>$updated_date], ['user_id' => $users['user_id']]);
					$this->response = [
						'status' => '1',
						'message' =>'Successfully! We update your new password',
					];
				}else{
					$this->response = [
						'status' => '0',
						'message' =>'Old password Wrong!',
					];	
				}
			}else{
				$this->response = [
					'status' => '0',
					'message' =>'User Not Available!',
				];
			}
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);
	}

	public function get_live_match_scorecard()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$cb_match_number = $_POST['cb_match_number'];

		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_HEADER, 0);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($curl, CURLOPT_URL, "http://www.cricbuzz.com/api/html/cricket-scorecard/".$cb_match_number);
				$html=curl_exec($curl);
				$dom = new simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
				$html = $dom->load($html, true, true);
					$data =  $html->find('div[id^="innings"]');
					$scorecard = array();
					foreach($data as $story){
							$tables = $story->find('div[class^="cb-col cb-col-100"]');
							$batsman_inning = array();
							$bowler_inning = array();
							foreach($tables as $table){
								if(isset($table->find('div[class^="cb-col cb-col-27"]',0)->plaintext)){
									if($table->find('div[class^="cb-col cb-col-27"]',0)->plaintext != 'Batsman'){
										if(isset($table->find('div[class^="cb-col cb-col-33"]',0)->plaintext)){
											$mali_bat = [
												'batsman'=>$table->find('div[class^="cb-col cb-col-27"]',0)->plaintext,
												'details'=>$table->find('div[class^="cb-col cb-col-33"]',0)->plaintext,
												'R'=>$table->find('div[class^="cb-col cb-col-8"]',0)->plaintext,
												'B'=>$table->find('div[class^="cb-col cb-col-8"]',1)->plaintext,
												'4s'=>$table->find('div[class^="cb-col cb-col-8"]',2)->plaintext,
												'6s'=>$table->find('div[class^="cb-col cb-col-8"]',3)->plaintext,
												'SR'=>$table->find('div[class^="cb-col cb-col-8"]',4)->plaintext,
											];
										array_push($batsman_inning,$mali_bat);    
										}
									}
								}
								if(isset($table->find('div[class^="cb-col cb-col-40"]',0)->plaintext)){
									if($table->find('div[class^="cb-col cb-col-40"]',0)->plaintext != 'Bowler'){
										$mali_bow = [
											'Bowler'=>$table->find('div[class^="cb-col cb-col-40"]',0)->plaintext,
											'O'=>$table->find('div[class^="cb-col cb-col-8"]',0)->plaintext,
											'M'=>$table->find('div[class^="cb-col cb-col-8"]',1)->plaintext,
											'R'=>$table->find('div[class^="cb-col cb-col-10"]',0)->plaintext,
											'W'=>$table->find('div[class^="cb-col cb-col-8"]',2)->plaintext,
											'NB'=>$table->find('div[class^="cb-col cb-col-8"]',3)->plaintext,
											'WD'=>$table->find('div[class^="cb-col cb-col-8"]',4)->plaintext,
											'ECO'=>$table->find('div[class^="cb-col cb-col-10"]',1)->plaintext,
										];
										array_push($bowler_inning,$mali_bow); 
									}
								}
							}
							$mali = [
								'innings'=>$story->find('div[class="cb-col cb-col-100 cb-scrd-hdr-rw"] span',0)->plaintext,
								'total_score'=>explode('&nbsp;',$html->find('div[class="cb-col cb-col-100 cb-scrd-hdr-rw"] span',1)->plaintext),
								'batsman'=>$batsman_inning,
								'bowler'=>$bowler_inning
							];
							array_push($scorecard,$mali);
					}
				$this->response = [
							'status' => '1',
							'message' =>'Successfully',
							'scorecard'=>$scorecard
						];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}	
		 echo json_encode($this->response);
	}

	public function get_match_players_points()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$cb_match_number = $_POST['cb_match_number'];
		$contest_id = $_POST['contest_id'];
		
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$match = $this->qm->select_where_row('tbl_matches',array('cb_match_number'=>$cb_match_number));
			$players_points_list = $this->qm->select_where('tbl_players_points',array('matches_id'=>$match['matches_id']));
			$matchessssss_currenttime = date('Y-m-d H:i:s');
			$matchessssss_updatetime =  date('Y-m-d H:i:s',strtotime($players_points_list[0]['updated_date']) + (120));
			if($matchessssss_currenttime >= $matchessssss_updatetime){
				
				if(isset($players_points_list[0])){
					if($match['status'] == '2'){
						$matcheeessss = $this->qm->select_where('tbl_players_points',array('matches_id'=>$match['matches_id']));
						$teamsssss = $this->qm->select_all('tbl_teams');
						$point_table = array();
						foreach($matcheeessss as $matcheeess){
							foreach($teamsssss as $teamsss){
								if($teamsss['teams_id'] == $matcheeess['teams_id']){
									$total_points = intval($matcheeess['batsman_points'])+intval($matcheeess['bowler_point'])+intval($matcheeess['fielding_points']);
									if($matcheeess['captain_details'] == 'c'){
										$total_points = $total_points * 2; 
									}
									if($matcheees['captain_details'] == 'vc'){
										$total_points = $total_points * 1.5;
									}
									$malisss = [
										'players_id'=>$matcheeess['players_id'],
										'teams_name' => $teamsss['teams_name'],
										'teamshort_name' => $teamsss['teamshort_name'],
										'players_name'=>$matcheeess['players_name'],
										'total_points'=>$total_points
									];
									array_push($point_table,$malisss);
								}
							}
						}
						$this->response = [
							'status' => '1',
							'message' =>'Successfully!',
							'teams1_score'=>$match['teams1_score'],
							'teams2_score'=>$match['teams2_score'],
							'point_table'=>$point_table
						];
					}else{
						
						$point_table_list = $this->qm->select_all('tbl_point_table');
						$this->qm->updt('tbl_players_points', ['updated_date'=>date('Y-m-d H:i:s')], array('matches_id'=>$match['matches_id']));
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_HEADER, 0);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
						curl_setopt($curl, CURLOPT_URL, "http://www.cricbuzz.com/api/html/cricket-scorecard/".$cb_match_number);
						$html=curl_exec($curl);
						$dom = new simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
						$html = $dom->load($html, true, true);
							$data =  $html->find('div[id^="innings"]');
							$scorecard = array();
							$matchessss_score = array();
							foreach($data as $story){
									$tables = $story->find('div[class^="cb-col cb-col-100"]');
										$innings = explode(' ',explode('Innings',$story->find('div[class="cb-col cb-col-100 cb-scrd-hdr-rw"] span',0)->plaintext)[0]);
										$total_score = explode('&nbsp;',$html->find('div[class="cb-col cb-col-100 cb-scrd-hdr-rw"] span',1)->plaintext);
										array_push($matchessss_score, $innings[0][0].''.$innings[1][0].''.$innings[2][0].' - '.$total_score[0].$total_score[1]);
									foreach($tables as $table){
										if(isset($table->find('div[class^="cb-col cb-col-27"]',0)->plaintext)){
											if($table->find('div[class^="cb-col cb-col-27"]',0)->plaintext != 'Batsman'){
												if(isset($table->find('div[class^="cb-col cb-col-33"]',0)->plaintext)){
													$player_namesss = explode(' ',$table->find('div[class^="cb-col cb-col-27"]',0)->plaintext);
													$playerssss_points = $this->db->select('*')->from('tbl_players_points')->where("matches_id = ".$match['matches_id']." AND players_name LIKE '%".$player_namesss[2]."%'")->get()->row();
													$player_pointsss_id = $playerssss_points->players_points_id;
													$batsman_points = 0;
													if(count($playerssss_points) > 0){
														
														$batsman_points = $batsman_points + (intval($table->find('div[class^="cb-col cb-col-8"]',0)->plaintext)*$point_table_list[0]['point_table_value']);
														$batsman_points = $batsman_points + (intval($table->find('div[class^="cb-col cb-col-8"]',2)->plaintext)*$point_table_list[1]['point_table_value']);
														$batsman_points = $batsman_points + (intval($table->find('div[class^="cb-col cb-col-8"]',3)->plaintext)*$point_table_list[2]['point_table_value']);
														
														if(intval($table->find('div[class^="cb-col cb-col-8"]',0)->plaintext) >= 50){
														$batsman_points = $batsman_points + intval(intval($table->find('div[class^="cb-col cb-col-8"]',0)->plaintext)/50)*$point_table_list[3]['point_table_value'];
														}
		
														if(intval($table->find('div[class^="cb-col cb-col-8"]',0)->plaintext) == 0){
														$batsman_points = $batsman_points - $point_table_list[5]['point_table_value'];
														}
														
														if(intval($table->find('div[class^="cb-col cb-col-8"]',4)->plaintext) >= 60 && intval($table->find('div[class^="cb-col cb-col-8"]',4)->plaintext) <= 70){
														$batsman_points = $batsman_points - $point_table_list[21]['point_table_value'];
														}
														if(intval($table->find('div[class^="cb-col cb-col-8"]',4)->plaintext) >= 50 && intval($table->find('div[class^="cb-col cb-col-8"]',4)->plaintext) <= 50.9){
														$batsman_points = $batsman_points - $point_table_list[22]['point_table_value'];
														}
														if(intval($table->find('div[class^="cb-col cb-col-8"]',4)->plaintext) < 50){
														$batsman_points = $batsman_points - $point_table_list[23]['point_table_value'];
														}
														if(strpos($table->find('div[class^="cb-col cb-col-33"]',0)->plaintext,'run out') !== false ) {
															$runout = explode('(',$table->find('div[class^="cb-col cb-col-33"]',0)->plaintext);
															$runout1 = explode(')',$runout[1])[0];                     
															$runout2 = explode('/',$runout1);
															if(isset($runout2[1])){
																$this->qm->updt('tbl_players_points', ['fielding_points'=>4], "matches_id = ".$match['matches_id']." AND players_name LIKE '%".$runout2[0]."%'");
																$this->qm->updt('tbl_players_points', ['fielding_points'=>2], "matches_id = ".$match['matches_id']." AND players_name LIKE '%".$runout2[1]."%'");
															}else{
																$this->qm->updt('tbl_players_points', ['fielding_points'=>6], "matches_id = ".$match['matches_id']." AND players_name LIKE '%".$runout2[0]."%'");
															}
														}
														$this->qm->updt('tbl_players_points', ['batsman_points'=>$batsman_points], array('players_points_id'=>$player_pointsss_id));
														
													}
												}
											}
										}
										if(isset($table->find('div[class^="cb-col cb-col-40"]',0)->plaintext)){
											if($table->find('div[class^="cb-col cb-col-40"]',0)->plaintext != 'Bowler'){
													$player_namesss = explode(' ',$table->find('div[class^="cb-col cb-col-40"]',0)->plaintext);
													$playerssss_points = $this->db->select('*')->from('tbl_players_points')->where("players_name LIKE '%".$player_namesss[2].' '.$player_namesss[3]."%'")->get()->row();
													$player_pointsss_id = $playerssss_points->players_points_id;
													$bowler_point = 0;
													if(count($playerssss_points)>0){
														$bowler_point = $bowler_point + (intval($table->find('div[class^="cb-col cb-col-8"]',2)->plaintext)*$point_table_list[6]['point_table_value']); // Wicket
														
														if(intval($table->find('div[class^="cb-col cb-col-8"]',2)->plaintext) >= 4){
															if(intval($table->find('div[class^="cb-col cb-col-8"]',2)->plaintext) >= 5){
																$bowler_point = $bowler_point + $point_table_list[8]['point_table_value']; // 5 wicket more bonus
															}else{
																$bowler_point = $bowler_point + $point_table_list[7]['point_table_value']; // 4 wicket more bonus
															}
														}
														$bowler_point = $bowler_point + (intval($table->find('div[class^="cb-col cb-col-8"]',1)->plaintext)*$point_table_list[9]['point_table_value']); // Maiden over
														if(intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) < 4){
														$bowler_point = $bowler_point + $point_table_list[15]['point_table_value']; 
														}
														if(intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) >= 4 && intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) <= 4.99){
														$bowler_point = $bowler_point + $point_table_list[16]['point_table_value']; 
														}
														if(intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) >= 5 && intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) <= 6){
														$bowler_point = $bowler_point + $point_table_list[17]['point_table_value']; 
														}
														if(intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) >= 9 && intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) <= 10){
														$bowler_point = $bowler_point - $point_table_list[18]['point_table_value']; 
														}
														if(intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) >= 10.1 && intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) <= 11){
														$bowler_point = $bowler_point - $point_table_list[19]['point_table_value']; 
														}
														if(intval($table->find('div[class^="cb-col cb-col-10"]',1)->plaintext) > 11){
														$bowler_point = $bowler_point - $point_table_list[20]['point_table_value']; 
														}
														$this->qm->updt('tbl_players_points', ['bowler_point'=>$bowler_point], array('players_points_id'=>$player_pointsss_id));
													}
											}
										}
									}
							}
							$this->qm->updt('tbl_matches', ['teams1_score'=>$matchessss_score[0],'teams2_score'=>$matchessss_score[1]], array('matches_id'=>$match['matches_id']));
							$matcheeess = $this->qm->select_where('tbl_players_points',array('matches_id'=>$match['matches_id']));
							$teamsss = $this->qm->select_all('tbl_teams');
							$point_table = array();
							foreach($matcheeess as $matcheees){
								foreach($teamsss as $teamss){
									if($teamss['teams_id'] == $matcheees['teams_id']){
										$total_points = intval($matcheees['batsman_points'])+intval($matcheees['bowler_point'])+intval($matcheees['fielding_points']);
										if($matcheees['captain_details'] == 'c'){
											$total_points = $total_points * 2; 
										}
										if($matcheees['captain_details'] == 'vc'){
											$total_points = $total_points * 1.5;
										}
										$malisss = [
											'players_id'=>$matcheees['players_id'],
											'teams_name' => $teamss['teams_name'],
											'teamshort_name' => $teamss['teamshort_name'],
											'players_name'=>$matcheees['players_name'],
											'total_points'=>$total_points
										];
										array_push($point_table,$malisss);
									}
								}
							}
							$this->response = [
								'status' => '1',
								'message' =>'Successfully!',
								'teams1_score'=>$match['teams1_score'],
								'teams2_score'=>$match['teams2_score'],
								'point_table'=>$point_table
							];
					}
				}else{
					
					$match1 = $match['teams_id1'];
					$match2 = $match['teams_id2'];
					$contest_participate = $this->qm->select_where_row('tbl_contest_participate',array('user_id'=>$user_id,'contest_id'=>$contest_id,'matches_id'=>$match['matches_id']));
					$teams_players = $this->db->query("SELECT * FROM tbl_players WHERE teams_id IN ($match1,$match2)")->result_array();
					foreach($teams_players as $teams_player){
						if($contest_participate['captain'] == $teams_player['players_id']){
							$captain_details = 'c';
						}elseif($contest_participate['vice_captain'] == $teams_player['players_id']){
							$captain_details = 'vc';
						}else{
							$captain_details = '';
						}
						$data = [
							'matches_id'=>$match['matches_id'],
							'teams_id' => $teams_player['teams_id'],
							'players_id' => $teams_player['players_id'],
							'players_name' => $teams_player['players_name'],
							'batsman_points' => 0,
							'bowler_point' => 0,
							'fielding_points' => 0,
							'captain_details' => $captain_details,
							'created_date' => date('Y-m-d H:i:s'),
							'updated_date' => date('Y-m-d H:i:s')
						];
							$insert_id = $this->qm->ins('tbl_players_points', $data);
							$matcheeess = $this->qm->select_where('tbl_players_points',array('matches_id'=>$match['matches_id']));
							$teamsss = $this->qm->select_all('tbl_teams');
							$point_table = array();
							foreach($matcheeess as $matcheees){
								foreach($teamsss as $teamss){
									if($teamss['teams_id'] == $matcheees['teams_id']){
										$total_points = intval($matcheees['batsman_points'])+intval($matcheees['bowler_point'])+intval($matcheees['fielding_points']);
										if($matcheees['captain_details'] == 'c'){
											$total_points = $total_points * 2; 
										}
										if($matcheees['captain_details'] == 'vc'){
											$total_points = $total_points * 1.5;
										}
										$malisss = [
											'players_id'=>$matcheees['players_id'],
											'teams_name' => $teamss['teams_name'],
											'teamshort_name' => $teamss['teamshort_name'],
											'players_name'=>$matcheees['players_name'],
											'total_points'=>$total_points
										];
										array_push($point_table,$malisss);
									}
								}
							}
							$this->response = [
								'status' => '1',
								'message' =>'Successfully!',
								'teams1_score'=>$match['teams1_score'],
								'teams2_score'=>$match['teams2_score'],
								'point_table'=>$point_table
							];
					}
				}
			}else{
				$matcheeess = $this->qm->select_where('tbl_players_points',array('matches_id'=>$match['matches_id']));
				$teamsss = $this->qm->select_all('tbl_teams');
				$point_table = array();
				foreach($matcheeess as $matcheees){
					foreach($teamsss as $teamss){
						if($teamss['teams_id'] == $matcheees['teams_id']){
							$total_points = intval($matcheees['batsman_points'])+intval($matcheees['bowler_point'])+intval($matcheees['fielding_points']);
							if($matcheees['captain_details'] == 'c'){
								$total_points = $total_points * 2; 
							}
							if($matcheees['captain_details'] == 'vc'){
								$total_points = $total_points * 1.5;
							}
							$malisss = [
								'players_id'=>$matcheees['players_id'],
								'teams_name' => $teamss['teams_name'],
								'teamshort_name' => $teamss['teamshort_name'],
								 'players_name'=>$matcheees['players_name'],
								'total_points'=>$total_points
							];
							array_push($point_table,$malisss);
						}
					}
				}
				$this->response = [
					'status' => '1',
					'message' =>'Successfully!',
			     	'teams1_score'=>$match['teams1_score'],
					'teams2_score'=>$match['teams2_score'],
					'point_table'=>$point_table
				];
			}
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}	
		 echo json_encode($this->response);
	}
	
	public function get_my_contest()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$date_current = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s')) - (5*3600));
			$date_next = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s')) + (100*3600));
			$sql = "SELECT tbl_matches.* FROM tbl_matches WHERE match_time BETWEEN '$date_current' AND '$date_next' ";
			$result_matches = $this->db->query($sql)->result_array();
			$response = array();
			foreach($result_matches as $result_match){
				$teams1 = $this->qm->select_where_row('tbl_teams',['teams_id'=>$result_match['teams_id1']]);
				$teams2=$this->qm->select_where_row('tbl_teams',array('teams_id'=>$result_match['teams_id2']));
				$contest_participate = $this->db->select('contest_id')->from('tbl_contest_participate')->where(array('user_id'=>$user_id,'matches_id'=>$result_match['matches_id']))->get()->result();
				$date_current1 = date('Y-m-d H:i:s');
				if($date_current1 < $result_match['match_time']){
					$match_current_status = 0;
				}else{
					$match_current_status = 1;
				}
				if(count($contest_participate)>0){
					$match_deails = [
						'matches_id' => $result_match['matches_id'],
						'cb_match_number' => $result_match['cb_match_number'],
						'match_type' => $result_match['match_type'],
						'status' => $match_current_status,
						'match_time' => $result_match['match_time'],
						'you_contest_joined'=>count($contest_participate),
						'contest_joined'=>$contest_participate,
						'teams1' => [
							'teams_id'=>$teams1['teams_id'],
							'teams_name'=>$teams1['teams_name'],
							'teamshort_name'=>$teams1['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams1['icon'],
						],
						'teams2' => [
							'teams_id'=>$teams2['teams_id'],
							'teams_name'=>$teams2['teams_name'],
							'teamshort_name'=>$teams2['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams2['icon'],
						]
					];
					array_push($response,$match_deails);
				}
			}
			$result_matches_close = $this->db->query("SELECT tbl_matches.* FROM tbl_matches WHERE status=2")->result_array();
			$close_matches_list = array();
			foreach($result_matches_close as $result_match_close){
				$teams_close1 = $this->qm->select_where_row('tbl_teams',['teams_id'=>$result_match_close['teams_id1']]);
				$teams_close2=$this->qm->select_where_row('tbl_teams',array('teams_id'=>$result_match_close['teams_id2']));
				$contest_participate_close = $this->db->select('contest_id')->from('tbl_contest_participate')->where(array('user_id'=>$user_id,'matches_id'=>$result_match_close['matches_id']))->get()->result();
				if(count($contest_participate_close)>0){
					$matches_close_deails = [
						'matches_id' => $result_match_close['matches_id'],
						'cb_match_number' => $result_match_close['cb_match_number'],
						'match_type' => $result_match_close['match_type'],
						'status' => $result_match_close['status'],
						'match_time' => $result_match_close['match_time'],
						'you_contest_joined'=>count($contest_participate_close),
						'contest_joined'=>$contest_participate_close,
						'teams1' => [
							'teams_id'=>$teams_close1['teams_id'],
							'teams_name'=>$teams_close1['teams_name'],
							'teamshort_name'=>$teams_close1['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams_close1['icon']
						],
						'teams2' => [
							'teams_id'=>$teams_close2['teams_id'],
							'teams_name'=>$teams_close2['teams_name'],
							'teamshort_name'=>$teams_close2['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams_close2['icon']
						]
					];
					array_push($close_matches_list,$matches_close_deails);
				}
			}

			$this->response = [
				'status' => '1',
				'message' =>'Successfull!',
				'data' => $response,
				'close_matches' => $close_matches_list
			];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);
	}

	public function save_participate_contest()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$player_list = $_POST['player_list'];
		$contest_id = $_POST['contest_id'];
		$matches_id = $_POST['matches_id'];
		$captain = $_POST['captain'];
		$vice_captain = $_POST['vice_captain'];

		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			if($player_list != '' && $contest_id != '' && $matches_id != '' && $captain != '' && $vice_captain != ''){
				$playersss = explode(',',$player_list);
				$matchhh = $this->qm->select_where_row('tbl_matches',array('matches_id'=>$matches_id));
				$close_contest_time =  date('Y-m-d H:i:s',strtotime($matchhh['match_time']) - (2*3600));
				 if(date('Y-m-d H:s:i') <= $close_contest_time){
					$teams1_playersss = $this->qm->select_where('tbl_players',array('teams_id'=>$matchhh['teams_id1']));
					$teams2_playersss = $this->qm->select_where('tbl_players',array('teams_id'=>$matchhh['teams_id2']));
					$teams11 = array();
					$teams22 = array();
					$WKK = array();
					$BATT = array();
					$ARR = array();
					$BOWLL = array();
					$total_players_credits = array();
					foreach($teams1_playersss as $teams1_players){
						if(in_array($teams1_players['players_id'],$playersss)){
							array_push($teams11,$teams1_players['players_id']);
							array_push($total_players_credits,$teams1_players['players_credits']);
							if($teams1_players['WK'] == '1'){
								array_push($WKK,$teams1_players['WK']);
							}
							if($teams1_players['BAT'] == '1'){
								array_push($BATT,$teams1_players['BAT']);
							}
							if($teams1_players['AR'] == '1'){
								array_push($ARR,$teams1_players['AR']);
							}
							if($teams1_players['BOWL'] == '1'){
								array_push($BOWLL,$teams1_players['BOWL']);
							}
						}
					}
					foreach($teams2_playersss as $teams2_players){
						if(in_array($teams2_players['players_id'],$playersss)){
							array_push($teams22,$teams2_players['players_id']);
							array_push($total_players_credits,$teams2_players['players_credits']);
							if($teams2_players['WK'] == '1'){
								array_push($WKK,$teams2_players['WK']);
							}
							if($teams2_players['BAT'] == '1'){
								array_push($BATT,$teams2_players['BAT']);
							}
							if($teams2_players['AR'] == '1'){
								array_push($ARR,$teams2_players['AR']);
							}
							if($teams2_players['BOWL'] == '1'){
								array_push($BOWLL,$teams2_players['BOWL']);
							}
						}
					}
					if(count($playersss) == 11){
						if(array_sum($total_players_credits) <= 100){
							if(count($teams11) <= 7){
								if(count($teams22) <= 7){
									if(count($WKK) == 1){
										if(count($BATT) <= 5 && count($BATT) >= 3){
											if(count($ARR) >= 1 && count($ARR) <= 3){
												if(count($BOWLL) >= 3 &&  count($BOWLL) <= 5){
													$contest = $this->qm->select_where('tbl_contest_participate',array('contest_id'=>$contest_id,'user_id'=>$user_id,'matches_id'=>$matches_id));
													$contest_list = $this->qm->select_where_row('tbl_contest',array('contest_id'=>$contest_id));
													if(count($contest_list)>0){
															if(count($contest)>0){
																$post_data14 = array(
																	'player_list' => $player_list,
																	'captain'=>$captain,
																	'vice_captain'=>$vice_captain,
																	'updated_date' => date('Y-m-d H:i:s')
																);
																$where = array('user_id' => $user_id,'contest_id' => $contest_id,'matches_id' => $matches_id);
																$this->qm->updt('tbl_contest_participate', $post_data14, $where);
																$this->response = [
																	'status' => '1',
																	'message' =>'Successfully!',
																	'data'=>[
																		'player_list'=>$player_list,
																		'captain'=>$captain,
																		'vice_captain'=>$vice_captain
																	]
																];
															}else{
																$post_data12 = array(
																	'user_id' => $user_id,
																	'contest_id' => $contest_id,
																	'matches_id' => $matches_id,
																	'player_list' => $player_list,
																	'captain' => $captain,
																	'vice_captain'=>$vice_captain,
																	'winner'=>'2',
																	'created_date' => date('Y-m-d H:i:s'),
																	'updated_date' => date('Y-m-d H:i:s')
																);
																$contest_participate_id = $this->qm->ins('tbl_contest_participate', $post_data12);
																if($contest_participate_id > 0){
																		$this->response = [
																			'status' => '1',
																			'message' =>'Successfully!',
																			'data'=>[	
																				'player_list'=>$player_list,
																				'captain'=>$captain,
																				'vice_captain'=>$vice_captain
																			]
																		];
																	
																}else{
																	$this->response = [
																		'status' => '0',
																		'message' =>'Error!',
																	];
																}
																
															}
													}else{
														$this->response = [
															'status' => '0',
															'message' =>'Error!',
														];
													}
												}else{
													$this->response = [
														'status' => '0',
														'message' =>'Between 3 to 5 players selection available For BOWL',
													];
												}
											}else{
												$this->response = [
													'status' => '0',
													'message' =>'Between 1 to 3 players selection available For AR',
												];
											}
										}else{
											$this->response = [
												'status' => '0',
												'message' =>'Maximum 3 OR 5 players selection available For BAT',
											];
										}
									}else{
										$this->response = [
											'status' => '0',
											'message' =>'Maximum 1 players selection available For WK',
										];
									}
								}else{
									$this->response = [
										'status' => '0',
										'message' =>'Maximum 7 players selection available2',
									];
								}
							}else{
								$this->response = [
									'status' => '0',
									'message' =>'Maximum 7 players selection available',
								];
							}
						}else{
							$this->response = [
								'status' => '0',
								'message' =>'Maximum 100 points available',
							];
						}
						
					}else{
						$this->response = [
							'status' => '0',
							'message' =>'Error!',
						];
					}
				}else{
					$this->response = [
						'status' => '0',
						'message' =>'Event close for this match',
					];
				}
			}else{
				$this->response = [
					'status' => '0',
					'message' =>'Error!',
				];
			}
			
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		
		echo json_encode($this->response);
	}

	public function get_teams_list()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$date_current = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s')) - (5*3600));
			$date_next = date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s')) + (100*3600));
			$sql = "SELECT tbl_matches.* FROM tbl_matches WHERE match_time BETWEEN '$date_current' AND '$date_next' ";
			$result_matches = $this->db->query($sql)->result_array();
			$response = array();
				$contest = $this->qm->select_all('tbl_contest');
				$contest_list = array();
				foreach($contest as $contest){
					$contest_prices = $this->qm->select_where('tbl_contest_price_list',array('contest_id'=>$contest['contest_id']));
					$contest_prices_list = array();
					foreach($contest_prices as $contest_price){
						$xcv = [
							'contest_price_id'=>$contest_price['contest_price_id'],
							'contest_list'=>$contest_price['contest_list'],
							'contest_price'=>$contest_price['contest_price']
						];
						array_push($contest_prices_list,$xcv);
					}
					$ccc = [
						'contest_id'=>$contest['contest_id'],
						'total_teams'=>$contest['users_teams'],
						'progress_bar'=>intval(100/$contest['users_teams']*rand(0,$contest['users_teams']-1)),
						'entory_fee'=>$contest['entory_fee'],
						'total_winners'=>$contest['total_winners'],
						'total_winnings_price'=>$contest['total_winnings_price'],
						'created_date'=>$contest['created_date'],
						'contest_winner_list'=>$contest_prices_list
						];
					array_push($contest_list,$ccc);
				}


			foreach($result_matches as $result_match){
				$teams1 = $this->qm->select_where_row('tbl_teams',['teams_id'=>$result_match['teams_id1']]);
				$teams2=$this->qm->select_where_row('tbl_teams',array('teams_id'=>$result_match['teams_id2']));
				$teams1_players = $this->qm->select_where_filter('tbl_players',array('teams_id'=>$result_match['teams_id1']),'position','DESC');
				$teams2_players = $this->qm->select_where_filter('tbl_players',array('teams_id'=>$result_match['teams_id2']),'position','DESC');
				$date_current1 = date('Y-m-d H:i:s');
				if($date_current1 < $result_match['match_time']){
					$match_current_status = 0;
				}else{
					$match_current_status = 1;
				}

				$match_deails = [
						'matches_id' => $result_match['matches_id'],
						'cb_match_number' => $result_match['cb_match_number'],
						'player_images_link'=>IMAGE.'players_icon/',
						'match_type' => $result_match['match_type'],
						'status' => $match_current_status,
						'match_time' => $result_match['match_time'],
						'teams1' => [
							'teams_id'=>$teams1['teams_id'],
							'teams_name'=>$teams1['teams_name'],
							'teamshort_name'=>$teams1['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams1['icon'],
							'players'=>$teams1_players
						],
						'teams2' => [
							'teams_id'=>$teams2['teams_id'],
							'teams_name'=>$teams2['teams_name'],
							'teamshort_name'=>$teams2['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams2['icon'],
							'players'=>$teams2_players
						]
				];
				array_push($response,$match_deails);
			}
			$result_matches_close = $this->db->query("SELECT tbl_matches.* FROM tbl_matches WHERE status=2")->result_array();
			$close_matches_list = array();
			foreach($result_matches_close as $result_match_close){
				$teams_close1 = $this->qm->select_where_row('tbl_teams',['teams_id'=>$result_match_close['teams_id1']]);
				$teams_close2=$this->qm->select_where_row('tbl_teams',array('teams_id'=>$result_match_close['teams_id2']));
				$matches_close_deails = [
						'matches_id' => $result_match_close['matches_id'],
						'cb_match_number' => $result_match_close['cb_match_number'],
						'player_images_link'=>IMAGE.'players_icon/',
						'match_type' => $result_match_close['match_type'],
						'status' => $result_match_close['status'],
						'match_time' => $result_match_close['match_time'],
						'teams1' => [
							'teams_id'=>$teams_close1['teams_id'],
							'teams_name'=>$teams_close1['teams_name'],
							'teamshort_name'=>$teams_close1['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams_close1['icon']
						],
						'teams2' => [
							'teams_id'=>$teams_close2['teams_id'],
							'teams_name'=>$teams_close2['teams_name'],
							'teamshort_name'=>$teams_close2['teamshort_name'],
							'teams_image'=>IMAGE.'teams_icon/'.$teams_close2['icon']
						]
				];
				array_push($close_matches_list,$matches_close_deails);
			}

			$this->response = [
				'status' => '1',
				'message' =>'Successfull!',
				'contest_list' => $contest_list,
				'data' => $response,
				'close_matches' => $close_matches_list
			];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);
	}

	public function get_chain_details()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$users = $this->qm->select_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1]);
		if (count($users) > 0) {
			$contests_history = $this->qm->select_where('tbl_contest_participate',['user_id'=>$users['user_id']]);
			$settings = $this->qm->select_where_row('tbl_setting',['id'=>'1']);
			$total_contest = array();
			$total_matches = array();
			$winners = array();
			foreach($contests_history as $contest_history){
				if(!in_array($contest_history['matches_id'],$total_matches)){
					array_push($total_matches,$contest_history['matches_id']);
				}
				if(!in_array($contest_history['contest_id'],$total_matches)){
					array_push($total_contest,$contest_history['contest_id']);
				}
				if($contest_history['winner'] == 1){
					array_push($winners,$contest_history['winner']);
				}
			}
			$referral_channel_users = $this->qm->select_where('tbl_users',['referral'=>$users['referral_key'],'status'=>'1']);
			$referral_chnnel = array();
			$total_max=0;
			$total_min=0;
				foreach($referral_channel_users as $referral_channel_user){
					$sub_users_contests_history = $this->qm->select_where('tbl_contest_participate',['user_id'=>$referral_channel_user['user_id']]);
					$sub_users_total_contest = array();
					$sub_users_total_matches = array();
					$sub_users_winners = array();
					foreach($sub_users_contests_history as $sub_users_contest_history){
						if(!in_array($sub_users_contest_history['contest_id'],$sub_users_total_contest)){
							array_push($sub_users_total_contest,$sub_users_contest_history['contest_id']);
						}
						if(!in_array($sub_users_contest_history['matches_id'],$sub_users_total_matches)){
							array_push($sub_users_total_matches,$sub_users_contest_history['matches_id']);
						}
						if($sub_users_contest_history['winner'] == 1){
							array_push($sub_users_winners,$sub_users_contest_history['winner']);
						}
					}
					$mesha = [
						'referral_key'=>$referral_channel_user['referral_key'],
						'username'=>$referral_channel_user['username'],
						'user_profile'=>$referral_channel_user['user_profile'],
						'referral_balance'=>[
							'max'=>$settings['join_referral'],
							'min'=>$referral_channel_user['referral_balance'],
							'process_bar'=>($referral_channel_user['referral_balance']*100)/$settings['join_referral']
						],
						'playing_history'=>[
							'contest'=>count($sub_users_total_contest),
							'matches'=>count($sub_users_total_matches),
							'winners'=>count($sub_users_winners),
							'lose'=>count($sub_users_total_contest) - count($sub_users_winners)
						]
					];
					$total_min = $total_min + $referral_channel_user['referral_balance'];
					$total_max = $total_max + $settings['join_referral'];
					array_push($referral_chnnel,$mesha);
				}
			$fvgb =[
					'referral_key'=>$users['referral_key'],
					'username'=>$users['username'],
					'user_profile'=>$users['user_profile'],
					'IMAGE_URL' => IMAGE . 'user_profile/',
					'referral_balance'=>[
						'max'=>$total_max,
						'min'=>$total_min,
						'process_bar'=>($total_min*100)/$total_max
					],
					'playing_history'=>[
						'contest'=>count($total_contest),
						'matches'=>count($total_matches),
						'winners'=>count($winners),
						'lose'=>count($total_contest) - count($winners),
					],
					'sub_channel'=>$referral_chnnel
				];
			$this->response = [
				'status' => '1',
				'message' =>'Successfull!',
				'data' => $fvgb
			];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Error!'
			];
		}
		echo json_encode(unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($this->response))));
	}

	public function get_add_cash_link()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$amount = $_POST['amount'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$token = $this->randomString_session();
			$this->response = [
				'status' => '1',
				'message' =>'Successfully!',
				'link' => URL.'Paypal/c_amt/'.$user_id.'/'.$amount.'/'.$token
			];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Error!'
			];
		}
		echo json_encode($this->response);
	}

	public function get_paypal_transaction()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
				$payments = $this->qm->select_where('tbl_payments',['user_id'=>$user_id]);
				$payments_txn = $this->qm->select_where('tbl_payments_txn',['user_id'=>$user_id]);
				$payment_history = array();
				foreach($payments as $payment){
					$debit = [
						'txn_id' => $payment['txn_id'],
						'txn_type' => 'debit',
						'amount' => $payment['payment_gross'],
						'currency_code' => $payment['currency_code'],
						'email'=> $payment['payer_email'],
						'status'=> $payment['payment_status'],
						'created_date'=> $payment['created_date'],
					];
					array_push($payment_history,$debit);
				}
				foreach($payments_txn as $payment_txn){
					$credit = [
						'txn_id' => $payment_txn['paypal_txn_id'],
						'txn_type' => 'credit',
						'amount' => $payment_txn['payment_gross'],
						'currency_code' => $payment_txn['currency_code'],
						'email'=> $payment_txn['payer_email'],
						'status'=> $payment_txn['payment_status'],
						'created_date'=> $payment_txn['created_date'],
					];
					array_push($payment_history,$credit);
				}
				$this->response = [
					'status' => '1',
					'message' =>'Successfully!',
					'data' => $payment_history
				];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Error!'
			];
		}
		echo json_encode($this->response);
	}

	public function submit_pancard_details()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
							$data['name_on_pancard'] = $_POST['name_on_pancard'];
							$data['pancard_number'] = $_POST['pancard_number'];
							$data['pancard_dob'] = $_POST['pancard_dob'];
							$data['pancard_verify_status'] = '0';
							if (isset($_POST['pancard_image']) && ($_POST['pancard_image']) != "") {
								$pancard_image = $_POST['pancard_image'];
								$binary=base64_decode($pancard_image);
								header('Content-Type: bitmap; charset=utf-8');
								$file_name = rand(1111111, 9999999).'.png';
								$file = fopen('./images/pancard_image/'.$file_name, 'wb');
								fwrite($file, $binary);
								fclose($file);
								$data['pancard_image'] = $file_name;
							}
					$this->qm->updt('tbl_users',$data,['user_id'=>$user_id]);
			$this->response = [
				'status' => '1',
				'message' =>'Successfully Uploded Your Pancard Doc.!',
			];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);
	}
	
	public function get_doc_status()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$users = $this->qm->select_where_row('tbl_users',['user_id'=>$user_id]);
			$dfgcvb = [
				'email_verify_status' => $users['email_verify_token_status'],
				'name_on_pancard'=>$users['name_on_pancard'],
				'pancard_number'=>$users['pancard_number'],
				'pancard_dob'=>$users['pancard_dob'],
				'pancard_verify_status'=>$users['pancard_verify_status'],
				'bank_account_number'=>$users['bank_account_number'],
				'bank_ifsc_code'=>$users['bank_ifsc_code'],
				'bank_verify_status'=>$users['bank_verify_status']
			];
			$this->response = [
				'status' => '1',
				'message' =>'Successfully!',
				'data'=> $dfgcvb
			];
		
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);
	}

	public function submit_bank_details()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
							$data['bank_account_number'] = $_POST['bank_account_number'];
							$data['bank_ifsc_code'] = $_POST['bank_ifsc_code'];
							$data['bank_verify_status'] = '0';
							if (isset($_POST['cheque_image']) && ($_POST['cheque_image']) != "") {
								$cheque_image = $_POST['cheque_image'];
								$binary=base64_decode($cheque_image);
								header('Content-Type: bitmap; charset=utf-8');
								$file_name = rand(1111111, 9999999).'.png';
								$file = fopen('./images/cheque_image/'.$file_name, 'wb');
								fwrite($file, $binary);
								fclose($file);
								$data['cheque_image'] = $file_name;
							}
			$this->qm->updt('tbl_users',$data,['user_id'=>$user_id]);
			$this->response = [
				'status' => '1',
				'message' =>'Successfully Uploded Your Bank Doc.!',
			];
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
		}
		echo json_encode($this->response);
	}

	public function doc_email_verification()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$email = $_POST['email'];
		if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
			$data['email_verify_token'] = $this->randomString_session();
			$data['email_verify_token_status'] = $_POST['bank_ifsc_code'];
			$this->qm->updt('tbl_users',$data,['user_id'=>$users['user_id']]);
			require_once(APPPATH."libraries/PHPMailer/PHPMailerAutoload.php");
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPDebug = 2;
			$mail->Debugoutput = 'html';
			$mail->Host = 'tls://smtp.gmail.com';
			$mail->Port = 587;
			$mail->SMTPSecure = 'tls';
			$mail->SMTPAuth = true;
			$mail->Username = "sandipkukadiya7start@gmail.com";
			$mail->Password = "!@#vRYVUsAvLbHlKbWP";
			$mail->setFrom('sandipkukadiya7start@gmail.com', 'Sandip Kukadiya');
			$mail->addAddress($email);
			$mail->Subject = 'Please verify your account';
			$mail->msgHTML("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
						<html>
						<head>
						<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
						<title>Please verify your account</title>
						</head>
						<body>
						<div style='width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;'>
						<h1>Welcome to the STAR11!</h1>
						<h3>Thank you for signing up</h3>
						<p>	Please verify your email address and activate your account. We’ll see you on the other side..</p>
						<a href='http://phpstack-165502-477873.cloudwaysapps.com/service/verify_users_email/".$data['email_verify_token']." '><strong>COMPLETE EMAIL VERIFICATION</strong></a>
						<p>If you did not sign up from our service, no further action is required.</p>
						</div>
						</body>
						</html>");
			$mail->AltBody = 'This is a plain-text message body';
			if($mail->send()){
				$this->response = [
					'status'=>1,
					'msg'=>'Successfully!'
				];
			}else{
				$this->response = [
					'status'=>0,
					'msg'=>'Error!'
				];
			}
			echo json_encode($this->response);
		}	
	}

	public function verify_users_email()
	{
		$email_verify_token = $this->uri->segment(3);
		$users = $this->qm->select_where_row('tbl_users',['email_verify_token'=>$email_verify_token,'status'=>1]);
		if(count($users) > 0){
			$this->qm->updt('tbl_users',array('email_verify_token_status'=>'1'),['user_id'=>$users['user_id']]);
			$this->response = 'Your email is successfully verify! Please go back to application';
			
		}else{
			$this->response = 'Your email is not verify yet! Please try again' ;
		}
		echo json_encode($this->response);
	}

	public function send_client_communication()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$msg = $_POST['msg'];
		$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		if(stripos($ua,'android') !== false) {
				if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
					$checkReq = $this->qm->num_where_row('tbl_comm',['user_id'=>$user_id,'create_date'=>date('Y-m-d')]);
					$setting = $this->qm->select_where_row('tbl_setting', ['id' => 1]);
					if($checkReq < $setting['ticket_request']) {
						$data = [
							'user_id' => $user_id,
							'msg' => $msg,
							'user'=>'1',
							'admin'=>'0',
							'status'=>'2',
							'create_date' => date('Y-m-d'),
							'create_time' => date('H:i:s')
						];
						$insert_id = $this->qm->ins('tbl_comm', $data);
						if($insert_id){
							$this->response = [
								'status' => '1',
								'message' => "Successfully! submit data"
							];
						}else{
							$this->response = [
								'status' => '2',
								'message' => "You are blocked, contact admin to active your account!"
							];	
						}
					}else{
						$this->response = [
							'status' => '3',
							'message' => "Request number is high. Please request tommorow!"
						];
					}			
					
				} else {
					$this->response = [
						'status' => '2',
						'message' => "You are blocked, contact admin to active your account!"
					];
				}
				echo json_encode($this->response);

		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
			echo json_encode($this->response);
		}
	}
	
	public function get_client_communication()
	{
		$user_id = $_POST['user_id'];
		$unique_id = $_POST['unique_id'];
		$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		if(stripos($ua,'android') !== false) {
			   if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
					$results = $this->db->query("SELECT * FROM tbl_comm WHERE user_id = $user_id")->result();
				
					$data = array();
					foreach($results as $result){
						if($result->status==1){$status = 'complete';}else{$status = 'pending';}
						$data1=[
							'msg'=>$result->msg,
							'user'=>$result->user,
							'admin'=>$result->admin,
							'status'=>$status,
							'datetime'=>$result->create_date.' '.$result->create_time
						];
						array_push($data,$data1);
					}
					$this->response = [
						'status' => '1',
						'message' => "Success!",
						'data' => $data
					];
				} else {
					$this->response = [
						'status' => '2',
						'message' => "You are blocked, contact admin to active your account!"
					];
				}
				echo json_encode($this->response);

		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
			echo json_encode($this->response);
		}
	}

	public function notification()
	{
		$user_id = $_REQUEST['user_id'];
		$unique_id = $_POST['unique_id'];

		$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		if(stripos($ua,'android') !== false) {
			if (($this->qm->num_where_row('tbl_users',['user_id'=>$user_id,'unique_id'=>$unique_id,'status'=>1])) > 0) {
				$sql = "SELECT noti_date as date,message as msg FROM tbl_notification";
				$data = $this->db->query($sql)->result();
				if ($data) {
					$this->response = [
						'status' => '1',
						'message' => "Success!",
						'data' => $data
					];
				} else {
					$this->response = [
						'status' => '0',
						'message' => "Failed!",
						'data' => array()
					];
				}
			}else{
				$this->response = [
					'status' => '2',
					'message' => "You are blocked, contact admin to active your account !"
				];
			}
			echo json_encode($this->response);
		}else{
			$this->response = [
				'status' => '0',
				'message' =>'Your devices not verify!',
			];
			echo json_encode($this->response);
		}
	}

	public function send_mail($email,$password) 
	{
		require_once(APPPATH."libraries/PHPMailer/PHPMailerAutoload.php");
		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPDebug = 2;
		$mail->Debugoutput = 'html';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;
		$mail->Username = "sandipkukadiya7start@gmail.com";
		$mail->Password = "!@#vRYVUsAvLbHlKbWP";
		$mail->setFrom('sandipkukadiya7start@gmail.com', 'Sandip Kukadiya');
		$mail->addAddress($email);
		$mail->Subject = 'New Password Request we sent you';
		$mail->msgHTML("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
					<html>
					<head>
					<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
					<title>New Password Request</title>
					</head>
					<body>
					<div style='width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;'>
					<h1>Hello User,</h1>
					<p>	You recently requested to reset your password for your STAR 11 Account. Get your new password from below.</p>
					<p>	<strong>".$password."</strong></p>
					</div>
					</body>
					</html>");
		$mail->AltBody = 'This is a plain-text message body';
		
		if (!$mail->send()) {
			return false;
		} else {
			return true;
		}
    }

	function is_luhn($n)
	{
		$str = '';
		foreach (str_split(strrev((string) $n)) as $i => $d) {
			$str .= $i %2 !== 0 ? $d * 2 : $d;
		}
		return array_sum(str_split($str)) % 10 === 0;
	}
	
	function is_imei($n)
	{
		return $this->is_luhn($n) && strlen($n) == 15;
	}
	function randomString_session()
	{
		$length = 64;
		$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$str = "";
		for ($i = 0; $i < $length; $i++) {
			$str .= $chars[mt_rand(0, strlen($chars) - 1)];
		}
		return $str;
	}
	function randomString_unique_id()
	{
		$length = 16;
		$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$str = "";
		for ($i = 0; $i < $length; $i++) {
			$str .= $chars[mt_rand(0, strlen($chars) - 1)];
		}
		return $str;
	}
	function randomString_refferal()
	{
		$length = 8;
		$chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$str = "";
		for ($i = 0; $i < $length; $i++) {
			$str .= $chars[mt_rand(0, strlen($chars) - 1)];
		}
		return $str;
	}
} 