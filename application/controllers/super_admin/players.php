<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Players extends CI_Controller {
    public function Players()
    {
        parent::__construct();
        $this->load->model('query_model','qm',TRUE);
        $this->load->helper('url');
        if (!isset($this->session->userdata['admin'])) {
            redirect('admin');
        }
	}
    public function index()
    {
        $data['records'] = $this->db->query('SELECT * FROM tbl_players ORDER BY position ASC')->result_array();
        $this->load->view('super_admin/header');
        $this->load->view('super_admin/players_list',$data);
        $this->load->view('super_admin/footer');
    }

    public function add_players()
    {   
        $this->load->view('super_admin/header');
        $players_id = $this->uri->segment(4);
        if ($players_id != "") {
            $where = array('players_id' => $players_id);
            if (isset($_POST['submit'])) {
                $post_data = array();
                if (isset($_FILES['icon']['name']) && ($_FILES['icon']['name']) != "") {
                    $data['tbl'] = 'tbl_players';
                    $data['select_field'] = 'icon';
                    $data['where_field'] = "players_id='".$players_id."'";
                    $imgpath = 'images/players_icon';
                    $data['img_path'] = glob($imgpath.'*');
                    $this->qm->delete_img($data);
                    $type = pathinfo($_FILES['icon']['name'], PATHINFO_EXTENSION);
                    $icon = rand(1111, 9999) . time() . "." . $type;
                    $config['file_name'] = $icon;
                    $config['upload_path'] = "images/players_icon/";
                    $config['allowed_types'] = "jpg|jpeg|png|bmp";
                    $this->upload->initialize($config);
                    $this->upload->do_upload('icon');
                    $post_data['icon'] = $icon;
                }
                $post_data['players_name'] = ucfirst($_POST['players_name']);
                $post_data['teams_id'] = ucfirst($_POST['teams_id']);
                $post_data['players_credits'] = ucfirst($_POST['players_credits']);
                $this->qm->updt('tbl_players', $post_data, $where);
                redirect('super_admin/players');
            }
            else
            {
                $view_data['app_data'] = $this->qm->select_where('tbl_players', $where);
                $this->load->view('super_admin/add_players', $view_data);
            }
        }
        else
        {
            if(isset($_POST['submit']))
            {   
                $icon = "";
                if (isset($_FILES['icon']['name']) && ($_FILES['icon']['name']) != "") {
                    $type = pathinfo($_FILES['icon']['name'], PATHINFO_EXTENSION);
                    $icon = rand(1111, 9999) . time() . "." . $type;
                    $config['file_name'] = $icon;
                    $config['upload_path'] = "images/players_icon/";
                    $config['allowed_types'] = "jpg|jpeg|png|bmp";
                    $this->upload->initialize($config);
                    $this->upload->do_upload('icon');
                }
                $post_data = array(
                    'players_name' => ucfirst($_POST['players_name']),
                    'teams_id' => $_POST['teams_id'],
                    'WK' => '0',
                    'BAT' => '0',
                    'AR' => '0',
                    'BOWL' => '0',
                    'players_credits' => $_POST['players_credits'] ? $_POST['players_credits'] : '0',
                    'players_points' => $_POST['players_points'] ? $_POST['players_points'] : '0',
                    'icon' => $icon,
                );
                $this->qm->ins('tbl_players', $post_data);
                redirect('super_admin/players');
            }
            $this->load->view('super_admin/add_players');
        }
        $this->load->view('super_admin/footer');
    }

    public function delete_players(){
        $players_id = $_POST['players_id'];
        $this->db->query("DELETE FROM tbl_players WHERE players_id=$players_id");
        echo '1';
    }
    public function update_players_status_WK(){
        $players_id = $_POST['players_id'];
        $where = array('players_id' => $players_id);
        $post_data['WK'] = ucfirst($_POST['WK']);
        $this->qm->updt('tbl_players', $post_data, $where);
    }
    public function update_players_status_BAT(){
        $players_id = $_POST['players_id'];
        $where = array('players_id' => $players_id);
        $post_data['BAT'] = ucfirst($_POST['BAT']);
        $this->qm->updt('tbl_players', $post_data, $where);
    }
    public function update_players_status_AR(){
        $players_id = $_POST['players_id'];
        $where = array('players_id' => $players_id);
        $post_data['AR'] = ucfirst($_POST['AR']);
        $this->qm->updt('tbl_players', $post_data, $where);
    }
    public function update_players_status_BOWl(){
        $players_id = $_POST['players_id'];
        $where = array('players_id' => $players_id);
        $post_data['BOWl'] = ucfirst($_POST['BOWl']);
        $this->qm->updt('tbl_players', $post_data, $where);
    }

    public function update_players_position(){
            $list = explode(',' , $_POST['list_order']);
            print_r($list);
            $i = 1 ;
            foreach($list as $id) {
                $this->qm->updt('tbl_players', array('position'=>$i), array('players_id'=>$id));
                $i++ ;
            }
    }
}