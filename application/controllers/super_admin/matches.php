<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Matches extends CI_Controller {
    public function Matches()
    {
        parent::__construct();
        $this->load->model('query_model','qm',TRUE);
        $this->load->helper('url');
        $this->load->library("simple_html_dom");
        if (!isset($this->session->userdata['admin'])) {
            redirect('admin');
        }
	}
    public function index()
    {
        $id = $this->uri->segment(4);
        if ($id != '') {
            $data['records'] = $this->qm->select_where('tbl_matches', array('status' => $id));
            $data['id'] = $id;
        } else {
            $data['records'] = $this->qm->select_where('tbl_matches',array('status' => '0'));
        }
        
        $this->load->view('super_admin/header');
        $this->load->view('super_admin/matches_list',$data);
        $this->load->view('super_admin/footer');
    }

    public function add_matches()
    {   
        $this->load->view('super_admin/header');
        $matches_id = $this->uri->segment(4);
        if ($matches_id != "") {
            $where = array('matches_id' => $matches_id);
            if (isset($_POST['submit'])) {
                $post_data = array(
                    'cb_match_number' => $_POST['cb_match_number'],
                    'teams_id1' => $_POST['teams_id1'],
                    'teams_id2' => $_POST['teams_id2'],
                    'match_type' => $_POST['match_type'], 
                    'status' => '0', 
                    'match_time' => $_POST['match_time']
                );
                $this->qm->updt('tbl_matches', $post_data, $where);
                redirect('super_admin/matches');
            }
            else
            {
                $view_data['app_data'] = $this->qm->select_where('tbl_matches', $where);
                $this->load->view('super_admin/add_matches', $view_data);
            }
        }
        else
        {
            if(isset($_POST['submit']))
            {   
                $post_data = array(
                    'cb_match_number' => $_POST['cb_match_number'],
                    'teams_id1' => $_POST['teams_id1'],
                    'teams_id2' => $_POST['teams_id2'],
                    'match_type' => $_POST['match_type'], 
                    'status' => '0', 
                    'match_time' => $_POST['match_time']
                );
                $this->qm->ins('tbl_matches', $post_data);
                redirect('super_admin/matches');
            }
            $this->load->view('super_admin/add_matches');
        }
        $this->load->view('super_admin/footer');
    }

    public function delete_matches(){
        $matches_id = $_POST['matches_id'];
        $this->db->query("DELETE FROM tbl_matches WHERE matches_id=$matches_id");
        echo '1';
    }

    public function close_matches()
    {
        $matches_id = $_POST['matches_id'];
        // set to winner in tbl_contest_participate

        // $players_points_list = $this->qm->select_where('tbl_players_points',array('matches_id'=>$matches_id));
        // $contest_participate_list = $this->qm->select_where('tbl_contest_participate',array('matches_id'=>$matches_id));
        $this->qm->updt('tbl_matches', array('status'=>'2'), array('matches_id'=>$matches_id));
        echo '1';
    }
}