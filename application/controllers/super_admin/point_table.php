<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Point_table extends CI_Controller {
    public function Point_table()
    {
        parent::__construct();
        $this->load->model('query_model','qm',TRUE);
        $this->load->helper('url');
        if (!isset($this->session->userdata['admin'])) {
            redirect('admin');
        }
	}
    public function index()
    {
        $data['records'] = $this->qm->select_all('tbl_point_table');
        $this->load->view('super_admin/header');
        $this->load->view('super_admin/point_table_list',$data);
        $this->load->view('super_admin/footer');
    }

    public function add_point_table()
    {   
        $this->load->view('super_admin/header');
        $point_table_id = $this->uri->segment(4);
        if ($point_table_id != "") {
            $where = array('point_table_id' => $point_table_id);
            if (isset($_POST['submit'])) {
                $post_data = array();
                $post_data['point_table_text'] = $_POST['point_table_text'];
                $post_data['point_table_key'] = $_POST['point_table_key'];
                $post_data['point_table_value'] = $_POST['point_table_value'];
                $post_data['point_table_pm'] = $_POST['point_table_pm'];
                $this->qm->updt('tbl_point_table', $post_data, $where);
                redirect('super_admin/point_table');
            }
            else
            {
                $view_data['app_data'] = $this->qm->select_where('tbl_point_table', $where);
                $this->load->view('super_admin/add_point_table', $view_data);
            }
        }
        else
        {
            if(isset($_POST['submit']))
            {   
                $icon = "";
                $post_data = array(
                    'point_table_text' => $_POST['point_table_text'],
                    'point_table_key' => $_POST['point_table_key'],
                    'point_table_value' => $_POST['point_table_value'],
                    'point_table_pm' => $_POST['point_table_pm']
                );
                $this->qm->ins('tbl_point_table', $post_data);
                redirect('super_admin/point_table');
            }
            $this->load->view('super_admin/add_point_table');
        }
        $this->load->view('super_admin/footer');
    }

    public function delete_point_table(){
        $point_table_id = $_POST['point_table_id'];
        $this->db->query("DELETE FROM tbl_point_table WHERE point_table_id=$point_table_id");
        echo '1';
    }
  
}
