<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends CI_Controller
{
    public function Setting()
    {
        parent::__construct();
        $this->load->model('query_model', 'qm', TRUE);
        $this->load->helper('url');
        if (!isset($this->session->userdata['admin'])) {
            redirect('admin');
        }
    }

    public function index()
    {
        if(!$this->session->userdata('pass')) {
            $this->load->view('super_admin/header');
            $this->load->view('super_admin/setting_login');
            $this->load->view('super_admin/footer');
        }else{
            $view_data['level'] = $this->qm->select_all('tbl_level');
            $view_data['setting'] = $this->qm->select_where_row('tbl_setting',['id'=>1]);
            $this->load->view('super_admin/header');
            $this->load->view('super_admin/setting', $view_data);
            $this->load->view('super_admin/footer');
        }
    }
    //8200526242Skk
    public function set_pass(){
        if(isset($_POST['enter'])){
            $where = array(
				'email' => $this->session->userdata('email')
			);
            $validate = $this->qm->select_where('tbl_admin', $where);
            if(password_verify($_POST['pass'], $validate[0]['password'])){
                $this->session->set_userdata('pass', $_POST['pass']);
                redirect('super_admin/setting', 'refresh');
            }else{
                redirect('super_admin/setting', 'refresh');
            }
        }
    }

    public function getUpdate(){
        $val = $_POST['val'];
        $key = $_POST['key'];
        $this->qm->updt("tbl_setting",array($key=>$val),array('id'=>1));
        //echo $val;
    }

    public function getReferaal(){
        $val = $_POST['val'];
        $id = $_POST['id'];
        $this->qm->updt("tbl_level",array('price'=>$val),array('level_id'=>$id));
    }
}