<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Teams extends CI_Controller {
    public function Teams()
    {
        parent::__construct();
        $this->load->model('query_model','qm',TRUE);
        $this->load->helper('url');
        if (!isset($this->session->userdata['admin'])) {
            redirect('admin');
        }
	}
    public function index()
    {
        $data['records'] = $this->qm->select_all('tbl_teams');
        $this->load->view('super_admin/header');
        $this->load->view('super_admin/teams_list',$data);
        $this->load->view('super_admin/footer');
    }

    public function add_teams()
    {   
        $this->load->view('super_admin/header');
        $teams_id = $this->uri->segment(4);
        if ($teams_id != "") {
            $where = array('teams_id' => $teams_id);
            if (isset($_POST['submit'])) {
                $post_data = array();
                if (isset($_FILES['icon']['name']) && ($_FILES['icon']['name']) != "") {
                    
                    $data['tbl'] = 'tbl_teams';
                    $data['select_field'] = 'icon';
                    $data['where_field'] = "teams_id='".$teams_id."'";
                    $imgpath = 'images/teams_icon';
                    $data['img_path'] = glob($imgpath.'*');
                    $this->qm->delete_img($data);
                    $type = pathinfo($_FILES['icon']['name'], PATHINFO_EXTENSION);
                    $icon = rand(1111, 9999) . time() . "." . $type;
                    $config['file_name'] = $icon;
                    $config['upload_path'] = "images/teams_icon/";
                    $config['allowed_types'] = "jpg|jpeg|png|bmp";
                    $this->upload->initialize($config);
                    $this->upload->do_upload('icon');
                    $post_data['icon'] = $icon;
                }
                $post_data['teams_name'] = ucfirst($_POST['teams_name']);
                $post_data['teamshort_name'] = ucfirst($_POST['teamshort_name']);
                $this->qm->updt('tbl_teams', $post_data, $where);
                redirect('super_admin/teams');
            }
            else
            {
                $view_data['app_data'] = $this->qm->select_where('tbl_teams', $where);
                $this->load->view('super_admin/add_teams', $view_data);
            }
        }
        else
        {
            if(isset($_POST['submit']))
            {   
                $icon = "";
                if (isset($_FILES['icon']['name']) && ($_FILES['icon']['name']) != "") {
                    $type = pathinfo($_FILES['icon']['name'], PATHINFO_EXTENSION);
                    $icon = rand(1111, 9999) . time() . "." . $type;
                    $config['file_name'] = $icon;
                    $config['upload_path'] = "images/teams_icon/";
                    $config['allowed_types'] = "jpg|jpeg|png|bmp";
                    $this->upload->initialize($config);
                    $this->upload->do_upload('icon');
                }
                $post_data = array(
                    'teams_name' => ucfirst($_POST['teams_name']),
                    'teamshort_name' => strtoupper($_POST['teamshort_name']),
                    'icon' => $icon,
                );
                $this->qm->ins('tbl_teams', $post_data);
                redirect('super_admin/teams');
            }
            $this->load->view('super_admin/add_teams');
        }
        $this->load->view('super_admin/footer');
    }

    public function delete_teams(){
        $teams_id = $_POST['teams_id'];
        $this->db->query("DELETE FROM tbl_teams WHERE teams_id=$teams_id");
        echo '1';
    }
}