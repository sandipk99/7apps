<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contest extends CI_Controller {
    public function Contest()
    {
        parent::__construct();
        $this->load->model('query_model','qm',TRUE);
        $this->load->helper('url');
        if (!isset($this->session->userdata['admin'])) {
            redirect('admin');
        }
	}
    public function index()
    {
        $data['records'] = $this->qm->select_all('tbl_contest');
        $this->load->view('super_admin/header');
        $this->load->view('super_admin/contest_list',$data);
        $this->load->view('super_admin/footer');
    }

    public function add_contest()
    {   
        $this->load->view('super_admin/header');
        $contest_id = $this->uri->segment(4);
        if ($contest_id != "") {
            $where = array('contest_id' => $contest_id);
            if (isset($_POST['submit'])) {
                $post_data = array(
                    'users_teams' => $_POST['users_teams'],
                    'entory_fee' => $_POST['entory_fee'],
                    'total_winners' => $_POST['total_winners'], 
                    'total_winnings_price' => $_POST['total_winnings_price'], 
                    'updated_date' => date('Y-m-d H:i:s')
                );
                $this->qm->updt('tbl_contest', $post_data, $where);
                $this->db->query("DELETE FROM tbl_contest_price_list WHERE contest_id=$contest_id");
                $contest_price = $_POST['contest_price'];
                for($i=0;$i<$_POST['total_winners'];$i++){
                    $post_data = array(
                        'contest_id' => $contest_id,
                        'contest_list' => $_POST['contest_list'][$i],
                        'contest_price' => $_POST['contest_price'][$i],
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $this->qm->ins('tbl_contest_price_list', $post_data);
                }
                redirect('super_admin/contest');
            }
            else
            {
                $view_data['app_data'] = $this->qm->select_where('tbl_contest', $where);
                $view_data['app_data11'] = $this->qm->select_where('tbl_contest_price_list', $where);
                $this->load->view('super_admin/add_contest', $view_data);
            }
        }
        else
        {
            if(isset($_POST['submit']))
            {   

                $contest_price = $_POST['contest_price'];
                $post_data = array(
                    'users_teams' => $_POST['users_teams'],
                    'entory_fee' => $_POST['entory_fee'],
                    'total_winners' => $_POST['total_winners'], 
                    'total_winnings_price' => $_POST['total_winnings_price'], 
                    'participate_users' => '0',
                    'status' => '0',
                    'created_date' => date('Y-m-d H:i:s')
                );
                $contest_id = $this->qm->ins('tbl_contest', $post_data);
                for($i=0;$i<$_POST['total_winners'];$i++){
                    $post_data = array(
                        'contest_id' => $contest_id,
                        'contest_list' => $_POST['contest_list'][$i],
                        'contest_price' => $_POST['contest_price'][$i],
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    $this->qm->ins('tbl_contest_price_list', $post_data);
                }
                redirect('super_admin/contest');
            }
            $this->load->view('super_admin/add_contest');
        }
        $this->load->view('super_admin/footer');
    }

    public function delete_contest(){
        $contest_id = $_POST['contest_id'];
        $this->db->query("DELETE FROM tbl_contest WHERE contest_id=$contest_id");
        $this->db->query("DELETE FROM tbl_contest_price_list WHERE contest_id=$contest_id");
        echo '1';
    }
}