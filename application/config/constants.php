<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('URL','http://phpstack-165502-477873.cloudwaysapps.com/');
//define('URL','http://localhost/star11/');
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define("ADMIN_TITLE","STAR 11");
define("IMAGE",URL."images/");
define("CSS",URL."css/");
define("JS",URL."js/");
define("PLUGINS",URL."plugins/");
define("ACTIVE_LINK",URL.'activeAccount');
define("OFFSET",'10');
define("firebase_key",'AIzaSyBb3qiSMLclq-ZhqIZTZxIxSfN1V3jiq50');

define("HOST","localhost");
define("USER","vwxkanfrcp");
define("PASS","7kzb9YntDV");
define("DB","vwxkanfrcp");

// Paytm Wallet

define('PAYTM_APP_NAME', 'STAR 11');
define('PAYTM_ENVIRONMENT', 'TEST'); 
define('PAYTM_MERCHANT_KEY', ''); 
define('PAYTM_MERCHANT_MID', ''); 
define('PAYTM_MERCHANT_GUID', ''); 
define('PAYTM_SALES_WALLET_GUID', '');
define('PAYTM_MERCHANT_WEBSITE', 'xxxxxxx'); 

$PAYTM_DOMAIN = "pguat.paytm.com";
$PAYTM_WALLET_DOMAIN = "trust-uat.paytm.in";
if (PAYTM_ENVIRONMENT == 'PROD') {
	$PAYTM_DOMAIN = 'secure.paytm.in';
	$PAYTM_WALLET_DOMAIN = "trust.paytm.in";
}

define('PAYTM_REFUND_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/REFUND');
define('PAYTM_STATUS_QUERY_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/TXNSTATUS');
define('PAYTM_TXN_URL', 'https://'.$PAYTM_DOMAIN.'/oltp-web/processTransaction');
define('PAYTM_GRATIFICATION_URL', 'https://'.$PAYTM_WALLET_DOMAIN.'/wallet-web/salesToUserCredit');

